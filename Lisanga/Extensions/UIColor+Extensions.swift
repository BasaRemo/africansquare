//
//  UIColor+Extensions.swift
//  OfficeHours
//
//  Created by Eliel Gordon on 12/7/15.
//  Copyright © 2015 Saltar Group. All rights reserved.
//

import UIKit

extension UIColor {
//    static func primaryBlueColor() -> UIColor {
//        return UIColor(red: 23/255, green: 64/255, blue: 126/255, alpha: 1.0)
//    }
//    
    static func primaryBlueColor() -> UIColor {
        return UIColor(red: 29/255, green: 85/255, blue: 141/255, alpha: 1.0)
    }
    
    static func lightBlueColor() -> UIColor {
        return UIColor(red: 29/255, green: 173/255, blue: 255/255, alpha: 1.0)
    }
    
    static func lighterGrayColor() -> UIColor {
        return UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1.0)
    }
    static func primaryGrayColor() -> UIColor {
        return UIColor(red: 19/255, green: 19/255, blue: 20/255, alpha: 1.0)
    }
    static func primaryGrayLightColor() -> UIColor {
        return UIColor(red: 45/255, green: 44/255, blue: 55/255, alpha: 0.5)
    }
    
    static func cellBackgroundColor() -> UIColor {
        return UIColor(red: 26/255, green: 26/255, blue: 28/255, alpha: 0.5)
    }
    static func cellSelectColor() -> UIColor {
        return  UIColor(red: 0/255 , green: 0/255, blue: 0/255, alpha: 0.2)
    }
    static func orangePositiveColor() -> UIColor {
        return hexColor("E46C01")
    }
    
    static func hexColor(hexString: String) -> UIColor {
        let hex = hexString.stringByTrimmingCharactersInSet(NSCharacterSet.alphanumericCharacterSet().invertedSet)
        var int = UInt32()
        NSScanner(string: hex).scanHexInt(&int)
        let a, r, g, b: UInt32
        switch hex.characters.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (1, 1, 1, 0)
        }
        
        return UIColor(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}

extension UIColor {
    
    convenience init(hexString: String) {
        let hex = hexString.stringByTrimmingCharactersInSet(NSCharacterSet.alphanumericCharacterSet().invertedSet)
        var int = UInt32()
        NSScanner(string: hex).scanHexInt(&int)
        let a, r, g, b: UInt32
        switch hex.characters.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (1, 1, 1, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}