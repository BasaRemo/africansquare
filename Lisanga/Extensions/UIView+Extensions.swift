//
//  UIView+Extensions.swift
//  OfficeHours
//
//  Created by Eliel Gordon on 12/17/15.
//  Copyright © 2015 Saltar Group. All rights reserved.
//

import UIKit
import Aspects

extension UIView {
    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
        
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.CGPath
        self.layer.mask = mask
        self.clipsToBounds = true
        
//        let radius = radius
//        let closure:((Void)->Void) = {
//            /* code here */
//            let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
//            let mask = CAShapeLayer()
//            mask.path = path.CGPath
//            self.layer.mask = mask
//            self.clipsToBounds = true
//        }
//        let block: @convention(block) Void -> Void = closure
//        let objectBlock = unsafeBitCast(block, AnyObject.self)
//        do { try aspect_hookSelector("layoutSublayers", withOptions: .PositionAfter, usingBlock: objectBlock) }
//        catch _ { /* ... */ }
        
    }
}

extension CALayer {
    // This will hold the keys for the runtime property associations
    private struct AssociationKey {
        static var CornerRect:Int8 = 1    // for the UIRectCorner argument
        static var CornerRadius:Int8 = 2  // for the radius argument
    }
    
    // new computed property on CALayer
    // You send the corners you want to round (ex. [.TopLeft, .BottomLeft])
    // and the radius at which you want the corners to be round
    var cornerRadii:(corners: UIRectCorner, radius:CGFloat) {
        get {
            let number = objc_getAssociatedObject(self, &AssociationKey.CornerRect)  as? NSNumber ?? 0
            let radius = objc_getAssociatedObject(self, &AssociationKey.CornerRadius)  as? NSNumber ?? 0
            return (corners: UIRectCorner(rawValue: number.unsignedLongValue), radius: CGFloat(radius.floatValue))
        }
        set (v) {
            let radius = v.radius
            let closure:((Void)->Void) = {
                let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: v.corners, cornerRadii: CGSize(width: radius, height: radius))
                let mask = CAShapeLayer()
                mask.path = path.CGPath
                self.mask = mask
            }
            let block: @convention(block) Void -> Void = closure
            let objectBlock = unsafeBitCast(block, AnyObject.self)
            objc_setAssociatedObject(self, &AssociationKey.CornerRect, NSNumber(unsignedLong: v.corners.rawValue), .OBJC_ASSOCIATION_RETAIN)
            objc_setAssociatedObject(self, &AssociationKey.CornerRadius, NSNumber(float: Float(v.radius)), .OBJC_ASSOCIATION_RETAIN)
            do { try aspect_hookSelector("layoutSublayers", withOptions: .PositionAfter, usingBlock: objectBlock) }
            catch _ { }
        }
    }
}