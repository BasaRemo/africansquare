//
//  ProfileView.swift
//  OfficeHours
//
//  Created by Eliel Gordon on 12/8/15.
//  Copyright © 2015 Saltar Group. All rights reserved.
//

import UIKit
import DOFavoriteButton

let ProfileViewCellIdentifier = "ProfileView"

class ProfileView: UICollectionReusableView {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    
    @IBAction func sharePressed(sender: AnyObject) {
        print("Share")
    }
    
    @IBAction func likePressed(sender: DOFavoriteButton) {
        if sender.selected {
            // deselect
            sender.deselect()
        } else {
            // select with animation
            sender.select()
        }
    }
    
//    func blurImage() {
//        let darkBlur = UIBlurEffect(style: UIBlurEffectStyle.Dark)
//        let blurView = UIVisualEffectView(effect: darkBlur)
//        blurView.frame = imageView.bounds
//        imageView.addSubview(blurView)
//    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        profileImage.roundCorners(.AllCorners, radius: profileImage.frame.size.width / 2)
        //        roundCorners(.AllCorners, radius: 6)
        //        blurImage()
    }
    
    static func nib() -> UINib {
        return UINib(nibName: ProfileViewCellIdentifier, bundle: nil)
    }
}
