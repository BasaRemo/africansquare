//
//  FilterMenuView.swift
//  Lisanga
//
//  Created by Professional on 2016-01-09.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit

protocol FilterViewDelegate {
    func filterBtnPressed(filter:FilterMenuView)
}
class FilterMenuView: UICollectionReusableView {

    private let items = ["restaurant","shop","beauty","waiter"]
    private let itemsLabel = ["Restaurant","Markets","Beauty","Waiter"]
    
    @IBOutlet var collectionView:UICollectionView!
    var categoryMenuDelegate: CategoryMenuDelegate?
    var selectedRow:Int = 0
    
    var delegate:FilterViewDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func filterBtnPressed(sender: AnyObject) {
        delegate?.filterBtnPressed(self)
    }
}

extension FilterMenuView:UICollectionViewDataSource,UICollectionViewDelegate {
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath)
        let cell = NSBundle.mainBundle().loadNibNamed("FilterMenuCell", owner: self, options: nil)[0] as! UICollectionViewCell
        
        let imageView: UIImageView = (cell.contentView.viewWithTag(10) as! UIImageView);
        imageView.image = UIImage(named:items[indexPath.row])
        //        imageView.image = imageView.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        
        let label:UILabel = (cell.contentView.viewWithTag(11) as! UILabel)
        label.text = itemsLabel[indexPath.row]
        
        imageView.tintColor = UIColor.whiteColor()
        label.textColor = UIColor.whiteColor()
        
        if selectedRow == indexPath.row {
            imageView.image = UIImage(named: "\(items[indexPath.row])_filled")
            imageView.image = imageView.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
            imageView.tintColor = UIColor.orangeColor()
            //            label.textColor = UIColor.orangeColor()
        }
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let cell = collectionView.cellForItemAtIndexPath(indexPath)!
        
        let imageView: UIImageView = (cell.contentView.viewWithTag(10) as! UIImageView);
        imageView.tintColor = UIColor.redColor()
        (cell.contentView.viewWithTag(11) as! UILabel).textColor = UIColor.redColor()
        
        selectedRow = indexPath.row
        collectionView.reloadData()
        //        collectionView.reloadItemsAtIndexPaths([indexPath])
        let category:String = itemsLabel[indexPath.row]
//        categoryMenuDelegate?.didSelectItemAtIndex(self, index: indexPath.row,category:category)
    }
    
}
