//
//  MainMenuCell.swift
//  Lisanga
//
//  Created by Professional on 2016-02-14.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit

class MainMenuCell: UITableViewCell {
    @IBOutlet var titleLabel:UILabel!
    @IBOutlet var subtitleLabel:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let selectedView = UIView()
        selectedView.backgroundColor = UIColor.cellSelectColor()
        selectedBackgroundView = selectedView
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
