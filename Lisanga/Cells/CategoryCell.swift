//
//  CategoryCell.swift
//  Lisanga
//
//  Created by Professional on 2015-11-05.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit


class CategoryCell: UITableViewCell {
    @IBOutlet var categoryLabel:UILabel!
    @IBOutlet var categoryIcon:UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
