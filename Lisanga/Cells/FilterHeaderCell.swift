//
//  FilterHeaderCell.swift
//  Lisanga
//
//  Created by Professional on 2016-01-09.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit

class FilterHeaderCell: UITableViewCell {

    @IBOutlet var titleLabel:UILabel!
    @IBOutlet var selectAlllabel:UILabel!
    
    var filterItem = FilterSectionItem() {
        didSet{
            self.titleLabel.text = filterItem.title
            if !filterItem.selected {
                selectAlllabel.text = "Select All"
            }else{
                selectAlllabel.text = "Deselect All"
            }
        }
    }
    
    func selectItem(){
        filterItem.selected = !filterItem.selected
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        self.backgroundColor = UIColor.redColor()
        let corners = UIRectCorner.TopLeft.union(UIRectCorner.TopRight)
//        self.roundCorners(corners, radius: 6)
        
        self.clipsToBounds = true
        self.layer.masksToBounds = true
        self.layer.cornerRadii = (corners, 6)
        
        self.contentView.clipsToBounds = true
        self.contentView.layer.masksToBounds = true
//        self.contentView.layer.cornerRadius = 6
    }

    @IBAction func btnPressed(sender: UIButton) {
        sender.selected = !sender.selected
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
