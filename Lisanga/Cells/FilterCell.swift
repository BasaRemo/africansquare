//
//  FilterCell.swift
//  Lisanga
//
//  Created by Professional on 2016-01-09.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit

class FilterCell: UITableViewCell {

    @IBOutlet var checkButton:UIButton!
    @IBOutlet var titleLabel:UILabel!
    @IBOutlet var iconImageView:UIImageView!
    
    var filterItem = FilterSectionItem() {
        didSet{
            self.titleLabel.text = filterItem.title
            iconImageView.image = UIImage(named: filterItem.iconPath )
            iconImageView.image = iconImageView.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
            self.titleLabel.textColor = UIColor.hexColor("6F7179")
            self.titleLabel.font = UIFont (name: "HelveticaNeue", size: 13)
            iconImageView.tintColor = UIColor.lightGrayColor()
            
            if !filterItem.selected{
                checkButton.selected = false
//                self.titleLabel.textColor = UIColor.hexColor("6F7179")
//                self.titleLabel.font = UIFont (name: "HelveticaNeue", size: 15)
//                iconImageView.tintColor = UIColor.lightGrayColor()
            }else{
                checkButton.selected = true
//                self.titleLabel.textColor = UIColor.orangeColor()
//                self.titleLabel.font = UIFont (name: "HelveticaNeue", size: 15)// UIFont.boldSystemFontOfSize(16)
//                iconImageView.tintColor = UIColor.orangePositiveColor()
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func selectItem(){
        filterItem.selected = !filterItem.selected
    }
    @IBAction func btnPressed(sender: UIButton) {
        
//        if !checkButton.selected {
//            self.titleLabel.textColor = UIColor.orangeColor()
//            self.titleLabel.font = UIFont.boldSystemFontOfSize(16)
//        }else{
//            self.titleLabel.textColor = UIColor.hexColor("6F7179")
//            self.titleLabel.font = UIFont.systemFontOfSize(15)
//        }
//        sender.selected = !sender.selected
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
//        if !checkButton.selected {
//            self.titleLabel.textColor = UIColor.greenColor()
//        }else{
//            self.titleLabel.textColor = UIColor.blackColor()
//        }
//        checkButton.selected = !checkButton.selected
        
    }
    
}

