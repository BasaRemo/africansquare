//
//  TutorialCell.swift
//  RWDevCon
//
//  Created by Mic Pringle on 27/02/2015.
//  Copyright (c) 2015 Ray Wenderlich. All rights reserved.
//

import UIKit

class InspirationCell: UICollectionViewCell {
  
  @IBOutlet private weak var imageView: UIImageView!
  @IBOutlet private weak var imageCoverView: UIView!
  @IBOutlet private weak var titleLabel: UILabel!
  @IBOutlet private weak var timeAndRoomLabel: UILabel!
  @IBOutlet private weak var speakerLabel: UILabel!
  
let blurView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.Dark))
    
  var inspiration: Inspiration? {
    didSet {
      if let inspiration = inspiration {
//        imageView.image = inspiration.backgroundImage
        titleLabel.text = inspiration.name
        timeAndRoomLabel.text = inspiration.home_country
        speakerLabel.text = inspiration.business_name
        
        inspiration.downloadImage()
        inspiration.inspirationImage.observeNew{ image in
            self.imageView.image = image
        }
        
        
      }
    }
  }
  
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let selectedView = UIView()
        selectedView.backgroundColor = UIColor.cellSelectColor()
        selectedBackgroundView = selectedView
        
//        let width = Int(CGRectGetWidth(self.frame))
//        let heigth = Int(CGRectGetHeight(self.frame))
//        let croppedImage = cropImage(UIImage(named: "africa")!, withWidth: width , andHeigth:heigth)
//        self.view.addSubview(UIImageView(image: croppedImage))
        
    }
    
  override func applyLayoutAttributes(layoutAttributes: UICollectionViewLayoutAttributes) {
    super.applyLayoutAttributes(layoutAttributes)
    
    let featuredHeight = UltravisualLayoutConstants.Cell.featuredHeight
    let standardHeight = UltravisualLayoutConstants.Cell.standardHeight
    
    blurView.frame = self.bounds
    blurView.frame.size.height = featuredHeight
//    self.contentView.addSubview(blurView)
//    self.contentView.bringSubviewToFront(self.timeAndRoomLabel)
//    self.contentView.bringSubviewToFront(self.speakerLabel)
//    self.contentView.bringSubviewToFront(self.imageCoverView)
//    self.contentView.bringSubviewToFront(self.titleLabel)
    
    let delta = 1 - ((featuredHeight - CGRectGetHeight(frame)) / (featuredHeight - standardHeight))
    
    let minAlpha: CGFloat = 0.4
    let maxAlpha: CGFloat = 0.85
    
    let blur_minAlpha: CGFloat = 0.0
    let blur_maxAlpha: CGFloat = 0.90
    
    imageCoverView.alpha = maxAlpha - (delta * (maxAlpha - minAlpha))
    blurView.alpha = blur_maxAlpha - (delta * (blur_maxAlpha - blur_minAlpha))
    
    let scale = max(delta, 0.6)
    titleLabel.transform = CGAffineTransformMakeScale(scale, scale)
    
    timeAndRoomLabel.alpha = delta
    speakerLabel.alpha = delta
  }
  
}
