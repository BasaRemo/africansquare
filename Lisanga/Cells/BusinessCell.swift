//
//  BusinessCell.swift
//  Lisanga
//
//  Created by Professional on 2015-10-17.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit
import Bond
import MapKit
import CoreLocation
import Parse
import MGSwipeTableCell

class BusinessCell: MGSwipeTableCell {

    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var distanceLabel: UILabel!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var logoImageView: UIImageView!
    @IBOutlet var countryImageView: UIImageView!
    @IBOutlet var contentLayerView:UIView!
    
    let insets = UIEdgeInsets(top: 10, left: 8, bottom: 10, right: 8)
    
    var business:Business? {
        didSet {
            
            if let business = business {
                //Set the business Name and Short Description
                self.nameLabel.text = business.name ?? "Template name"
                business.categoryList = business.businessType!.componentsSeparatedByString(",")
                self.descriptionLabel.text = business.categoryList[0] as String ?? "Template Descr"
                
                business.businessHoursList = business.businessHours!.componentsSeparatedByString(", ")
                // bind the image of the post to the 'logoImageView
                business.image.observeNew{ image in
                    self.logoImageView.image = image
                    //business.businessImage = image
                }
                
                downloadImage(business)
                getBusinessDistanceFromUser(business)
                //getBusinessCategories(business) -- Will use the string chain naive solution for now
            }
        }
    }
    
    
    // Download the business image
    func downloadImage(business:Business){
        if !business.imageDownloaded{
            business.downloadImage()
            business.imageDownloaded = true
        }
    }
    
    // Get the business position from the current user
    func getBusinessDistanceFromUser(business:Business){
        
        PFGeoPoint.geoPointForCurrentLocationInBackground { (geoPoint: PFGeoPoint?, error: NSError?) -> Void in
            if error == nil {
                let userLocation = geoPoint
                let distance = userLocation?.distanceInKilometersTo(business.location)
                if let distance = distance {
//                    print("Distance: \(distance.roundToPlaces(1))")
                    self.distanceLabel.text = "\(distance.roundToPlaces(1)) km"
                }
            }
        }
    }
    
    // If we haven't fetched the categories yet..do it -- UNUSED FOR NOW. We are going with the string chain solution bcz it requires less queries
    func getBusinessCategories(business:Business){
    
        if business.categories.count == 0 {
            ParseHelper.sharedInstance.getCategoriesForBusiness(business, completionBlock: {
                (results: [PFObject]?, error: NSError?) -> Void in
                if error == nil {
                    // The find succeeded.
                    let categoriesList = results as? [Category] ?? []
                    business.categories = categoriesList
                    if business.categories.count > 0 {
                        //                                let cat = categoriesList[0]
                        //                                let mainCat:MainCategory = cat.toMainCategory!
                        //                                print("Main: \(mainCat.objectForKey("name")!)")
                        let mainCat = categoriesList[0]["toMainCategory"]["name"] as! String
                        self.descriptionLabel.text = mainCat ?? "Template Descr"
                    }
                    
                } else {
                    // Log details of the failure
                    print("Error: \(error!) \(error!.userInfo)")
                }
            })
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let selectedView = UIView()
        selectedView.backgroundColor = UIColor.cellSelectColor()
        selectedBackgroundView = selectedView
        
//        logoImageView.roundCorners(.AllCorners, radius: logoImageView.frame.size.width / 2)
//        layer.cornerRadius = 4
        logoImageView.layer.borderColor = UIColor.lighterGrayColor().CGColor
        logoImageView.layer.borderWidth = 1
        logoImageView.layer.cornerRadius = 4
        logoImageView.clipsToBounds = true
        
        clipsToBounds = true
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 4
        
        var frame = self.frame
        frame.origin.x = self.frame.origin.x + 10
//        frame.size.width = CGRectGetWidth(self.frame) - 5
        self.contentView.frame = frame
        self.frame = frame
        
        self.contentView.layer.cornerRadius = 4
        self.contentView.clipsToBounds = true
        self.contentView.frame.offsetInPlace(dx: 10, dy: 0)
        
        contentLayerView.layer.cornerRadius = 4
//        self.backgroundColor = UIColor.primaryGrayColor()
        
        
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
