//
//  SwitchCell.swift
//  Lisanga
//
//  Created by Professional on 2016-01-09.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit

class SwitchCell: UITableViewCell {

    @IBOutlet var titleLabel:UILabel!
    
    var filterItem = FilterSectionItem() {
        didSet{
            self.titleLabel.text = filterItem.title
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func btnPressed(sender: UIButton) {
        sender.selected = !sender.selected
    }
    
    @IBAction func switchPressed(sender: UISwitch) {
    }
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
