//
//  HeaderCell.swift
//  Lisanga
//
//  Created by Professional on 2016-01-09.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit

class HeaderCell: UITableViewCell {

    @IBOutlet var titleLabel:UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
