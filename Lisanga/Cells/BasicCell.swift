//
//  BasicCell.swift
//  Lisanga
//
//  Created by Professional on 2015-10-18.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit

class BasicCell: UITableViewCell {
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
