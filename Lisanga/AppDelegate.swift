//
//  AppDelegate.swift
//  Lisanga
//
//  Created by Professional on 2015-10-17.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit
import Parse
import Bolts

//let THEME_COLOR:UIColor = UIColor(red: 50/255 , green: 183/255, blue: 155/255, alpha: 1.0)
//let MENU_COLOR:UIColor = UIColor(red: 26/255 , green: 72/255, blue: 108/255, alpha: 1.0)
let THEME_COLOR:UIColor = UIColor(red: 29/255 , green: 83/255, blue: 126/255, alpha: 1.0)
let LABEL_COLOR:UIColor = UIColor.whiteColor()

func backgroundThread(delay: Double = 0.0, background: (() -> Void)? = nil, completion: (() -> Void)? = nil) {
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0)) {
        if(background != nil){ background!(); }
        
        let popTime = dispatch_time(DISPATCH_TIME_NOW, Int64(delay * Double(NSEC_PER_SEC)))
        dispatch_after(popTime, dispatch_get_main_queue()) {
            if(completion != nil){ completion!(); }
        }
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        //Parse.enableLocalDatastore()
//        application.statusBarHidden = true
        // Initialize Parse.
        Parse.setApplicationId("Tn34qWG6OioPgUQq1QPuS26z11C19byWyAPiDzkY",
            clientKey: "EJs8NBGCGiKFOAyQTRQJljWBwUJnz5h8bMIJ6kNr")
        
        Smooch.initWithSettings(SKTSettings(appToken: "1kyocjcj05h3f23zesu8e9o2b"))
    
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

