//
//  CustomClusterPin.swift
//  Lisanga
//
//  Created by Professional on 2015-10-18.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import TSClusterMapView

class CustomClusterPin: TSRefreshedAnnotationView {
    var label: UILabel!
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        self.image = UIImage(named: "ClusterAnnotation")
        self.frame = CGRectMake(0, 0, self.image!.size.width, self.image!.size.height)
        self.label = UILabel(frame: self.frame)
        self.label.font = UIFont.systemFontOfSize(10)
        self.label.textAlignment = NSTextAlignment.Center
        self.label.textColor = UIColor(red: 0/255 , green: 159/255, blue: 214/255, alpha: 1.0)
        self.label.center = CGPointMake(self.image!.size.width/2, self.image!.size.height * 0.43);
        self.centerOffset = CGPointMake(0, -self.frame.size.height/2);
        
        self.addSubview(self.label)
        self.canShowCallout = true
        
        self.clusteringAnimation()
    }
    override init(frame: CGRect) {

        super.init(frame: frame)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override func clusteringAnimation(){
        let clusterAnnotation:ADClusterAnnotation = self.annotation as! ADClusterAnnotation
        let count:Int = Int(clusterAnnotation.clusterCount)
        self.label.text = String(numberLabelText(Float(count)))
    }
    
    func numberLabelText(count:Float) ->Float{
        
        if (count > 1000) {
            var rounded:Float
            if (count < 10000) {
                rounded = ceilf(count/100)/10;
                return rounded
            }
            else {
                rounded = roundf(count/1000);
                return rounded;
            }
        }
        
        return count
    }
    

}
