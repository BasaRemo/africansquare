//
//  MainMenuVC.swift
//  Lisanga
//
//  Created by Professional on 2016-02-13.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit
import Toucan

func cropImage(image:UIImage, withWidth width:Int, andHeigth heigth:Int) -> UIImage{
    
    return Toucan(image: image).resize(CGSize(width: width, height: heigth), fitMode: Toucan.Resize.FitMode.Crop).image
}

class MainMenuVC: UIViewController {

    @IBOutlet var tableView:UITableView!
    var titles = ["BUSINESSES","INSPIRATIONS","MY PROFILE"]
    var subtitles = ["Find Local African Businesses","Discover African Entrepreneurs","Save and Manage you favorites"]
    var backgrounds = ["business_back","entrepreneur_back","profile_back"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.view.backgroundColor = UIColor.clearColor()
        self.navigationController?.navigationBar.barTintColor = UIColor.clearColor()
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
        self.navigationItem.title = "SANGA"
    }
    
}

// Mark - TABLE VIEW
extension MainMenuVC: UITableViewDataSource {
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("MainMenuCell", forIndexPath: indexPath) as! MainMenuCell
        
        cell.titleLabel.text = titles[indexPath.row]
        cell.subtitleLabel.text = subtitles[indexPath.row]
//        cell.contentView.backgroundColor = UIColor(patternImage: UIImage(named: backgrounds[indexPath.row])!)
        
        let width = Int(CGRectGetWidth(cell.frame))
        let heigth = Int(CGRectGetHeight(cell.frame))
        let croppedImage = cropImage(UIImage(named: backgrounds[indexPath.row])!, withWidth: width , andHeigth:heigth)
        cell.addSubview(UIImageView(image: croppedImage))

        let darkBlur = UIBlurEffect(style: UIBlurEffectStyle.Dark)
        let blurView = UIVisualEffectView(effect: darkBlur)
        blurView.frame = cell.bounds
        cell.addSubview(blurView)
        cell.bringSubviewToFront(cell.contentView)
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return titles.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return (UIScreen.mainScreen().bounds.height - 60 )/3
    }
    
}

extension MainMenuVC: UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let cell =  self.tableView.cellForRowAtIndexPath(indexPath) as! MainMenuCell
        cell.selected = false
        
        switch indexPath.row {
            case 0:
                let viewController = self.storyboard?.instantiateViewControllerWithIdentifier("MainVC") as! MainVC
                navigationController?.pushViewController(viewController, animated: true)
                self.navigationItem.title = ""
    
            case 1:
                let viewController = self.storyboard?.instantiateViewControllerWithIdentifier("InspirationsVC") as! InspirationsVC
                navigationController?.pushViewController(viewController, animated: true)
                self.navigationItem.title = ""
            case 2:
                break
            default:break
        }
        
        
        
    }    
}

