//
//  MainVC.swift
//  Lisanga
//
//  Created by Professional on 2015-12-25.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit
import SVProgressHUD
import ZFDragableModalTransition

var currentBusiness:Business?
protocol MainSeachBarDelegate {
    func didSelectSearchBar(vc: MainVC)
    func didCancelSearchBar(vc: MainVC)
}

func OBSERVE_SearchBarChange(viewController: UIViewController){
    NSNotificationCenter.defaultCenter().addObserver(viewController, selector: "ReceivedSearchBarNotification:", name:"SearchNotification", object: nil)
}

func NOTIFY_SearchBarChange(barState:Bool) {
    NSNotificationCenter.defaultCenter().postNotificationName("SearchNotification", object: barState)
}

class MainVC: UIViewController {

    @IBOutlet var magicButton:UIButton!
    @IBOutlet var mapContainerView:UIView!
    @IBOutlet var commerceContainerView:UIView!
    
    var rigthBarButtonItem: UIBarButtonItem!
    var leftBarButtonItem: UIBarButtonItem!
    
    var animator:ZFModalTransitionAnimator!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
            leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back_button"), style: UIBarButtonItemStyle.Plain, target: self, action: "leftBarBtnPressed")
            navigationItem.setLeftBarButtonItem(leftBarButtonItem, animated: true)
        
            rigthBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Search, target: self, action: "rightBarBtnPressed")
            navigationItem.setRightBarButtonItem(rigthBarButtonItem, animated: true)
        
            setupAppearance()
    }

    
    func setupWebVC(){
        
    }
    
    
    func leftBarBtnPressed() {
        
//        let mainMenuVC = self.storyboard?.instantiateViewControllerWithIdentifier("MainMenuVC") as! MainMenuVC
//        
//        self.animator = ZFModalTransitionAnimator(modalViewController: mainMenuVC)
//        self.animator.dragable = true
//        self.animator.bounces = true
//        self.animator.behindViewAlpha = 0.5
//        self.animator.behindViewScale = 1.0
//        self.animator.transitionDuration = 0.7
//        self.animator.direction = ZFModalTransitonDirection.Bottom
//        
//        mainMenuVC.transitioningDelegate = self.animator
//        mainMenuVC.modalPresentationStyle = UIModalPresentationStyle.Custom
//        
////        mainMenuVC.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
//        
//        self.presentViewController(mainMenuVC, animated: true, completion: nil)
        print("here")
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func rightBarBtnPressed() {
        let searchVC = self.storyboard?.instantiateViewControllerWithIdentifier("SearchVC") as! SearchVC
        self.presentViewController(searchVC, animated: true, completion: nil)
    }
    
    func setupAppearance(){
        
        // Code to hide the separtor between navbar and the view
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        //        self.navigationController?.navigationBar.clipsToBounds = true
        
        // Change appearance
        self.navigationController?.navigationBar.translucent = true
//        self.navigationController?.navigationBar.barStyle = .Black
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
        self.navigationItem.title = "SANGA"
        
        self.navigationController?.view.backgroundColor = UIColor.clearColor()
        self.navigationController?.navigationBar.barTintColor = UIColor.clearColor()
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        let width = Int(CGRectGetWidth(view.frame))
        let heigth = Int(CGRectGetHeight(view.frame))
        let croppedImage = cropImage(UIImage(named: "africa")!, withWidth: width , andHeigth:heigth)
        self.view.addSubview(UIImageView(image: croppedImage))
        
        let darkBlur = UIBlurEffect(style: UIBlurEffectStyle.Dark)
        let blurView = UIVisualEffectView(effect: darkBlur)
        blurView.frame = self.view.bounds
        self.view.addSubview(blurView)
        
        self.view.bringSubviewToFront(self.mapContainerView)
        self.view.bringSubviewToFront(self.commerceContainerView)
        self.mapContainerView.alpha = 0
        self.view.bringSubviewToFront(self.magicButton)
    }
    
    @IBAction func magicBtnPressed() {
        
        if (!magicButton.selected) {
//        //Show map
//            self.mapContainerView.alpha = 1
            UIView.animateWithDuration(0.3, delay: 0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                self.mapContainerView.alpha = 1
                self.commerceContainerView.alpha = 0
                self.commerceContainerView.hidden = true
            }, completion: { finished in
//                self.mapContainerView.hidden = false
            })
        
        }else{
        //Show list
            UIView.animateWithDuration(0.3, delay: 0, options: UIViewAnimationOptions.CurveLinear, animations: {
                self.commerceContainerView.alpha = 1
                self.commerceContainerView.hidden = false
                self.mapContainerView.alpha = 0
                }, completion: { finished in
//                    self.mapContainerView.hidden = true
//                    self.commerceContainerView.hidden = false
            })
        }
        
        magicButton.selected = !magicButton.selected
    }
    
}

extension Double {
    /// Rounds the double to decimal places value
    func roundToPlaces(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return round(self * divisor) / divisor
    }
}
