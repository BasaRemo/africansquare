//
//  MapVC.swift
//  Lisanga
//
//  Created by Professional on 2015-10-18.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import TSClusterMapView
import SwiftyJSON
import CoreLocation
import Parse
import Bolts

let CDStreetlightsJsonFile = "CDStreetlights"
var currentLoc: PFGeoPoint = PFGeoPoint(latitude: 45.519266,longitude: -73.6996583)
class MapVC: UIViewController {
    
    @IBOutlet var mapView: TSClusterMapView!
    var annotationArray: [MapPin]!
    var query: PFQuery = Business.query()!
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
//        queryData()
    }
    
    func queryData(){
        
        ParseHelper.sharedInstance.getParseBusiness(DEFAULT_RANGE, distanceFilter: DEFAULT_DISTANCE, completionBlock: {
            (posts, error) -> Void in
            if error == nil {
//                print("Success")
                let myPosts:[Business] = posts! as? [Business] ?? []
                
                for post in myPosts {
                    
                    if let point:PFGeoPoint = post.location {
                        
                        let coordinate: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: point.latitude, longitude: point.longitude)
                        let workoutClassName: String = post.objectForKey("name") as! String
                        let workoutClassInstructor: String = post.objectForKey("name") as! String
                        //
                        let annotation: MapPin = MapPin(coordinate: coordinate, title: "\(workoutClassName), \(workoutClassInstructor)", subtitle: "\(workoutClassName)")
                        //                    self.mapView.addAnnotation(annotation)
                        self.annotationArray.append(annotation)
                        
                    }
                }
                self.mapView.addClusteredAnnotations(self.annotationArray)
            } else {
                // Log details of the failure
                print("Error: \(error)")
            }
        })
        
        print("Current Location: \(currentLoc)")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self

//        mapView.setRegion(MKCoordinateRegionMake(CLLocationCoordinate2DMake(48.857617, 2.338820), MKCoordinateSpanMake(1.0, 1.0)), animated: true)
        let operationQueue = NSOperationQueue()
        let operation1 : NSBlockOperation = NSBlockOperation (block: {
//            self.loadData()
            self.queryData()
        })
        operationQueue.addOperation(operation1)
        annotationArray = [MapPin]()
        
//        manager = CLLocationManager()
        mapView.showsUserLocation = true
        
        let latitude: CLLocationDegrees = (LocationHelper.sharedInstance.manager.location?.coordinate.latitude)!
        let longitude: CLLocationDegrees = (LocationHelper.sharedInstance.manager.location?.coordinate.longitude)!
        let latDelta:CLLocationDegrees = 0.3
        let lonDelta:CLLocationDegrees = 0.3
        
        let span:MKCoordinateSpan = MKCoordinateSpanMake(latDelta, lonDelta)
        let location:CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
        let region:MKCoordinateRegion = MKCoordinateRegionMake(location, span)
        
        mapView.setRegion(region, animated: false)
        
        
    }

}
// MARK: - MKMapViewDelegate
extension MapVC: MKMapViewDelegate{
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        print("MKMapViewDelegate")
        let view:MKAnnotationView = MKAnnotationView.init(annotation: annotation, reuseIdentifier: NSStringFromClass(MKAnnotationView).componentsSeparatedByString(".").last!)
        view.image = UIImage(named: "pin_museum")
        view.canShowCallout = true
        view.centerOffset = CGPointMake(view.centerOffset.x, -view.frame.size.height/2)
        return view
    }
    
}
// MARK: - TSClusterMapViewDelegate
extension MapVC: TSClusterMapViewDelegate{
    

    func mapView(mapView: TSClusterMapView!, viewForClusterAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
        print("TSClusterMapViewDelegate")
        let view:CustomClusterPin = CustomClusterPin.init(annotation: annotation, reuseIdentifier: NSStringFromClass(CustomClusterPin).componentsSeparatedByString(".").last!)
        return view
    }
//    func mapView(mapView: TSClusterMapView!, willBeginBuildingClusterTreeForMapPoints annotations: Set<NSObject>!) {
//        <#code#>
//    }
//    func mapView(mapView: TSClusterMapView!, didFinishBuildingClusterTreeForMapPoints annotations: Set<NSObject>!) {
//        <#code#>
//    }
    
    func mapViewWillStartLoadingMap(mapView: MKMapView) {
        print("Start loading map")
    }
    func mapViewDidFinishLoadingMap(mapView: MKMapView) {
        print("End loading map")
    }
    
    func mapViewWillBeginClusteringAnimation(mapView: TSClusterMapView!) {
        print("Start cluster animation")
    }
    func mapViewDidFinishClusteringAnimation(mapView: TSClusterMapView!) {
        print("End cluster animation")
    }
    func userDidPanMapView(mapView: TSClusterMapView!) {
        print("User did Pan mapView ")
    }
}

