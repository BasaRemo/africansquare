//
//  ReviewVC.swift
//  Lisanga
//
//  Created by Professional on 2016-01-15.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit

class ReviewVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let px = 1 / UIScreen.mainScreen().scale
        let frame = CGRectMake(0, 0, self.view.frame.size.width, 0.3)
        let line: UIView = UIView(frame: frame)
        self.view.addSubview(line)
        line.backgroundColor = UIColor.lightGrayColor()
        
    }

}
