//
//  InspirationsViewController.swift
//  RWDevCon
//
//  Created by Mic Pringle on 02/03/2015.
//  Copyright (c) 2015 Ray Wenderlich. All rights reserved.
//

import UIKit
import Parse
import ZFDragableModalTransition

class InspirationsVC: UICollectionViewController {
  
    var inspirations = [Inspiration]()
//    var inspirations = Inspiration.allInspirations()
    var animator:ZFModalTransitionAnimator!
    let currentRange = 0...20
  override func preferredStatusBarStyle() -> UIStatusBarStyle {
    return UIStatusBarStyle.LightContent
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    if let patternImage = UIImage(named: "Pattern") {
      view.backgroundColor = UIColor(patternImage: patternImage)
    }
    collectionView!.backgroundColor = UIColor.clearColor()
    collectionView!.decelerationRate = UIScrollViewDecelerationRateFast
    
//    let lpgr = UILongPressGestureRecognizer(target: self, action: "handleLongPress:")
//    lpgr.minimumPressDuration = 0.2
////    lpgr.delaysTouchesBegan = true
//    lpgr.delegate = self
//    self.collectionView!.addGestureRecognizer(lpgr)
    
    self.loadInRange(currentRange) { mInspirations in
        self.inspirations = mInspirations!
        self.collectionView?.reloadData()
    }
  }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
            setupNavBar()
            collectionView?.reloadData()
        self.reloadInputViews()
    }
    func setupNavBar(){
        
        // Code to hide the separtor between navbar and the view
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
//        self.navigationController?.navigationBar.shadowImage = UIImage()
//        self.navigationController?.navigationBar.clipsToBounds = true
        
        // Change appearance
        self.navigationController?.navigationBar.translucent = true
        self.navigationController?.navigationBar.barStyle = .Black
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
        self.navigationItem.title = "Inspirations"
        
        self.navigationController?.view.backgroundColor = UIColor.clearColor()
        self.navigationController?.navigationBar.barTintColor = UIColor.clearColor()
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
    }

}

extension InspirationsVC {
  
  override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
    return 1
  }
  
  override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return inspirations.count
  }
  
  override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCellWithReuseIdentifier("InspirationCell", forIndexPath: indexPath) as! InspirationCell
    cell.inspiration = inspirations[indexPath.item]
    return cell
  }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let layout = collectionViewLayout as! UltravisualLayout
        let offset = layout.dragOffset * CGFloat(indexPath.item)
        if collectionView.contentOffset.y != offset {
            collectionView.setContentOffset(CGPoint(x: 0, y: offset), animated: true)
        }
    }

}

extension InspirationsVC {
    
    func loadInRange(range: Range<Int>, completionBlock: ([Inspiration]?) -> Void) {
        
        ParseHelper.sharedInstance.getParseInspirationEntrepreneurs(range) {
            (results: [PFObject]?, error: NSError?) -> Void in
            if error == nil {
                
                let inspirationsArray = results as? [Inspiration] ?? []
//                self.inspirations = inspirationsArray
                
                completionBlock(inspirationsArray)
                
            } else {
                // Log details of the failure
                print("Error: \(error!) \(error!.userInfo)")
            }
        }
    }
    
}

//extension InspirationsVC: UIGestureRecognizerDelegate{
//    
//    func handleLongPress(gestureReconizer: UILongPressGestureRecognizer) {
////        if gestureReconizer.state != UIGestureRecognizerState.Ended {
////            return
////        }
//        
//        let p = gestureReconizer.locationInView(self.collectionView)
//        let indexPath = self.collectionView!.indexPathForItemAtPoint(p)
//        
//        if let index = indexPath {
//            let cell = self.collectionView!.cellForItemAtIndexPath(index)
//            // do stuff with your cell, for example print the indexPath
//            print(index.row)
//            let layout = collectionViewLayout as! UltravisualLayout
//            if index.row == layout.featuredItemIndex{
//                self.openLectureView()
//            }
//            
//            
//        } else {
//            print("Could not find index path")
//        }
//    }
//    func openLectureView(){
//        let modalVC = self.storyboard?.instantiateViewControllerWithIdentifier("ViewController") as? ViewController
//        self.animator = ZFModalTransitionAnimator(modalViewController: modalVC)
//        self.animator.dragable = true
//        self.animator.bounces = false
//        self.animator.behindViewAlpha = 0.5
//        self.animator.behindViewScale = 1.0
//        self.animator.transitionDuration = 0.7
//        self.animator.direction = ZFModalTransitonDirection.Bottom
//        modalVC!.transitioningDelegate = self.animator
//        modalVC!.modalPresentationStyle = UIModalPresentationStyle.Custom
//        
//        presentViewController(modalVC!, animated: true, completion: nil)
////        self.navigationController!.pushViewController(modalVC!, animated: true)
//        
//    }
//}