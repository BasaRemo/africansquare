//
//  WebViewVC.swift
//  Lisanga
//
//  Created by Professional on 2016-01-05.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit

class WebViewVC: UIViewController {
    @IBOutlet  var webView: UIWebView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    var webUrl:String = "https://www.google.com"
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        webView.delegate = self
        let url = NSURL(string: webUrl)
        let requestObj = NSURLRequest(URL: url!)
        
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        webView.loadRequest(requestObj)
    }
    
    @IBAction func doRefresh() {
        webView.reload()
    }
    
    @IBAction func goBack() {
        webView.goBack()
    }
    
    @IBAction func goForward() {
        webView.goForward()
    }
    
    @IBAction func stop() {
        webView.stopLoading()
    }
}

extension WebViewVC:UIWebViewDelegate{
    func webViewDidFinishLoad(webView: UIWebView) {
        activityIndicator.stopAnimating()
    }
}