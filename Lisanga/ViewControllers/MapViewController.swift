//
//  MapViewController.swift
//  Lisanga
//
//  Created by Professional on 2015-12-29.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import TSClusterMapView
import SwiftyJSON
import CoreLocation
import Parse
import Bolts

class MapViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    var businessList = [Business]()
    var query: PFQuery = PFQuery(className: "Business")
    var manager:CLLocationManager!
    @IBOutlet var collectionView:UICollectionView!
    var annotationArray = [MapPin]()
    
    var itemsLabel = ["Ela Jambo", "Mama Africa", "OPCC", "Steph Odia","Congo Mikili", "Jeune Afrique", "Gentlemen 1st", "PrimeTime Training"]
    
    var items = ["01","02","03","04","05","06","07","08"]
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)

//        query.whereKey("dateOfClass", greaterThanOrEqualTo: NSDate())
//        query.whereKey("location", nearGeoPoint: currentLoc, withinMiles: 400)
        
        query.findObjectsInBackgroundWithBlock {
            (posts, error) -> Void in
            if error == nil {
//                print("Success")
                let myPosts = posts! as [PFObject] ?? []
                
                for (index, post) in myPosts.enumerate() {
                    
                    let point = post["location"] as! PFGeoPoint

                    let coordinate: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: point.latitude, longitude: point.longitude)
                    let workoutClassName: String = post.objectForKey("name") as! String
                    let workoutClassInstructor: String = post.objectForKey("name") as! String
//                    
                    let annotation: MapPin = MapPin(coordinate: coordinate, title: "\(workoutClassName), \(workoutClassInstructor)", subtitle: "\(workoutClassName)")
                    let customAnnotation = MyAnnotation(coordinate: coordinate,
                        title: "\(workoutClassName), \(workoutClassInstructor)",
                        subtitle: "\(workoutClassName)",
                        cardIndex: index)
                    
                    self.mapView.addAnnotation(customAnnotation)
                    self.annotationArray.append(annotation)

                }
//                self.mapView.addAnnotations(self.annotationArray)
            } else {
                // Log details of the failure
                print("Error: \(error)")
            }
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        setupMapView()
    }
    
    func setupMapView(){
        
        manager = CLLocationManager()
        manager.startUpdatingLocation()
        manager.requestWhenInUseAuthorization()
        manager.requestAlwaysAuthorization()
        
        mapView.showsUserLocation = true
        
        let latitude: CLLocationDegrees = (LocationHelper.sharedInstance.manager.location?.coordinate.latitude)!
        let longitude: CLLocationDegrees = (LocationHelper.sharedInstance.manager.location?.coordinate.longitude)!
        
        let latDelta:CLLocationDegrees = 0.3
        let lonDelta:CLLocationDegrees = 0.3
        
        let span:MKCoordinateSpan = MKCoordinateSpanMake(latDelta, lonDelta)
        let location:CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
        let region:MKCoordinateRegion = MKCoordinateRegionMake(location, span)
        
        mapView.setRegion(region, animated: false)
        mapView.delegate = self
    }

    func locationManager(manager: CLLocationManager,
        didChangeAuthorizationStatus status: CLAuthorizationStatus) {
            _ = false
            }
    
    func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation) {

    }
    
}

extension MapViewController: MKMapViewDelegate{
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        
        if (annotation is MKUserLocation) {
            return nil
        }
        let annotationM = annotation as? MyAnnotation
        
        var label: UILabel!
        let reuseId = "test"
        
        var view = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseId)
        if view == nil {
            
            view = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            view!.image = UIImage(named:"ClusterAnnotation")
            view!.canShowCallout = true
            //              self.image = UIImage(named: "ClusterAnnotation")
            view!.frame = CGRectMake(0, 0, view!.image!.size.width, view!.image!.size.height)
            label = UILabel(frame: view!.frame)
            label.font = UIFont.systemFontOfSize(10)
            label.textAlignment = NSTextAlignment.Center
            label.textColor = UIColor(red: 0/255 , green: 159/255, blue: 214/255, alpha: 1.0)
            label.center = CGPointMake(view!.image!.size.width/2, view!.image!.size.height * 0.43);
            view!.centerOffset = CGPointMake(0, -view!.frame.size.height/2);
            
            label.text = "\(annotationM!.indexCard!)"
            view!.addSubview(label)
        }
        else {
            view!.annotation = annotation
        }
        return view
        
//        let view:MKAnnotationView = MKAnnotationView.init(annotation: annotation, reuseIdentifier: NSStringFromClass(MKAnnotationView).componentsSeparatedByString(".").last!)
//        view.image = UIImage(named: "pin_museum")
//        view.canShowCallout = true
//        view.centerOffset = CGPointMake(view.centerOffset.x, -view.frame.size.height/2)
//        return view
    }
    
}

extension MapViewController:UICollectionViewDataSource,UICollectionViewDelegate {
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("MapCardCell", forIndexPath: indexPath)
        cell.layer.cornerRadius = 4
        cell.clipsToBounds = true
        
        let imageView: UIImageView = (cell.contentView.viewWithTag(10) as! UIImageView);
        imageView.image = UIImage(named:items[indexPath.row])
        
        let label:UILabel = (cell.contentView.viewWithTag(11) as! UILabel)
        label.text = itemsLabel[indexPath.row]
        
        let label1:UILabel = (cell.contentView.viewWithTag(12) as! UILabel)
        label1.text = itemsLabel[indexPath.row]
        
        let cardNumberLabel:UILabel = (cell.contentView.viewWithTag(13) as! UILabel)
        cardNumberLabel.text = "\(indexPath.row)"
//        cardNumberLabel.layer.cornerRadius = 12
//        cardNumberLabel.clipsToBounds = true
        
        return cell
    }
    

    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
//        let cell = collectionView.cellForItemAtIndexPath(indexPath)!
        
        let annotation: MapPin = self.annotationArray[indexPath.row]
        let coordinate: CLLocationCoordinate2D = annotation.coordinate
        self.mapView.setCenterCoordinate(coordinate, animated: true)
        
        //        var mapPoint = MKMapPointForCoordinate(coordinate)
        //        let annotationList = self.mapView.annotationsInMapRect(MKMapRect(origin: mapPoint, size: MKMapSizeMake(1,1)))
        //        print("Annotations count: \(annotationList.count)")
        //
        //        let myCustomAnnotation = MyAnnotationWithColor(coordinate: coordinate,
        //            title: annotation.title!,
        //            subtitle: annotation.subtitle!,
        //            pinColor:  .Purple)
        //
        //        for var annotation in annotationList{
        //            let annotationM = annotation as? MyAnnotation
        //            if annotationM?.coordinate.latitude == coordinate.latitude && annotationM?.coordinate.longitude == coordinate.longitude {
        //                print("anot: \(annotationM?.coordinate)")
        //                self.mapView.removeAnnotation(annotationM!)
        //                self.annotationArray.removeAtIndex(indexPath.row)
        //                self.mapView.addAnnotation(myCustomAnnotation)
        //            }
        //        }

    }

}
