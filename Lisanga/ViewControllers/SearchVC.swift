//
//  SearchVC.swift
//  Lisanga
//
//  Created by Professional on 2016-02-13.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit
import CarbonKit

class SearchVC: UIViewController {
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var containerView:UIView!
    var presentationVC:PresentationVC!
    var businessListVC:BusinessListVC!
    var carbonTabSwipeNavigation:CarbonTabSwipeNavigation!
    
    // this view can be in two different states
    enum State {
        case DefaultMode
        case SearchMode
    }
    
    // whenever the state changes, perform one of the two queries and update the list
    var state: State = .DefaultMode {
        didSet {
            switch (state) {
            case .DefaultMode:
                searchBar.resignFirstResponder() // 3
                searchBar.text = ""
                searchBar.showsCancelButton = false
                
            case .SearchMode:
                searchBar.setShowsCancelButton(true, animated: true)
                let searchText = searchBar?.text ?? ""
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        containerView.backgroundColor = UIColor.clearColor()
        
        let textFieldInsideSearchBar = searchBar.valueForKey("searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = UIColor.whiteColor()
        
//        var textFieldInsideSearchBarLabel = textFieldInsideSearchBar!.valueForKey("placeholderLabel") as? UILabel
//        textFieldInsideSearchBarLabel?.textColor = UIColor.whiteColor()
        // Do any additional setup after loading the view.
//        presentationVC = self.storyboard?.instantiateViewControllerWithIdentifier("PresentationVC") as! PresentationVC
//        businessListVC = self.storyboard?.instantiateViewControllerWithIdentifier("BusinessListVC") as! BusinessListVC
        
        let width = Int(CGRectGetWidth(view.frame))
        let heigth = Int(CGRectGetHeight(view.frame))
        let croppedImage = cropImage(UIImage(named: "africa")!, withWidth: width , andHeigth:heigth)
        self.view.addSubview(UIImageView(image: croppedImage))
        
        let darkBlur = UIBlurEffect(style: UIBlurEffectStyle.Dark)
        let blurView = UIVisualEffectView(effect: darkBlur)
        blurView.frame = self.view.bounds
        self.view.addSubview(blurView)
        
        setupCarbonKitTab()
        self.view.bringSubviewToFront(self.searchBar)
        self.view.bringSubviewToFront(self.containerView)
        
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        searchBar.becomeFirstResponder()
        state = .SearchMode
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        searchBar.resignFirstResponder()
        state = .DefaultMode
    }
}

// MARK: Searchbar Delegate
extension SearchVC: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
        state = .SearchMode
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.text = ""
        searchBar.setShowsCancelButton(false, animated: true)
        state = .DefaultMode
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
//        delegate?.searchSongWithText(searchText)
    }
}

// MARK: - CarbonTabSwipeNavigation Delegate
extension SearchVC:CarbonTabSwipeNavigationDelegate{
    
    func setupCarbonKitTab(){
        
        let tabMenuItems = ["Business","People"]
        
        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: tabMenuItems, delegate: self)
        carbonTabSwipeNavigation.insertIntoRootViewController(self.childViewControllers.first!)
        
        carbonTabSwipeNavigation.toolbar.translucent = false
        carbonTabSwipeNavigation.toolbar.backgroundColor = UIColor.clearColor()
        carbonTabSwipeNavigation.setIndicatorColor(UIColor.whiteColor())
        carbonTabSwipeNavigation.toolbarHeight.constant = 40
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        let width = screenSize.width/2
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(width, forSegmentAtIndex: 0)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(width+2, forSegmentAtIndex: 1)
        
        // Custimize segmented control
        carbonTabSwipeNavigation.setNormalColor(UIColor.hexColor("D2D2D2"), font: UIFont.systemFontOfSize(14))
        carbonTabSwipeNavigation.setSelectedColor(UIColor.whiteColor(), font: UIFont.systemFontOfSize(14))
        carbonTabSwipeNavigation.view.backgroundColor = UIColor.whiteColor()
        carbonTabSwipeNavigation.carbonSegmentedControl?.superview?.backgroundColor = UIColor.cellSelectColor()
        carbonTabSwipeNavigation.carbonSegmentedControl?.backgroundColor = UIColor.cellSelectColor()
//        for (var i:UInt = 0; i < 1; i++){
//            carbonTabSwipeNavigation.currentTabIndex = i
//            carbonTabSwipeNavigation.currentTabIndex = 0
//        }
//    
    }
    
    func carbonTabSwipeNavigation(carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAtIndex index: UInt) -> UIViewController {
        return UIViewController()
//        switch index {
//            case 0:
//                return businessListVC
//            case 1:
//                return presentationVC
//            default:return businessListVC
//            
//        }
    }
    
    func carbonTabSwipeNavigation(carbonTabSwipeNavigation: CarbonTabSwipeNavigation, willMoveAtIndex index: UInt) {
        
    }
    
    func carbonTabSwipeNavigation(carbonTabSwipeNavigation: CarbonTabSwipeNavigation, didMoveAtIndex index: UInt) {
        
    }
    
    func barPositionForCarbonTabSwipeNavigation(carbonTabSwipeNavigation: CarbonTabSwipeNavigation) -> UIBarPosition {
        return UIBarPosition.Top
    }
    
}
