//
//  ProfileHeaderVC.swift
//  Lisanga
//
//  Created by Professional on 2015-12-22.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit
import SABlurImageView

protocol ProfileHeaderDelegate {
    func backButtonTapped()
    
}

class ProfileHeaderVC: UIViewController {

    var profileHeaderDelegate:ProfileHeaderDelegate?
    
    @IBOutlet var imageView:SABlurImageView!
    @IBOutlet var headerView:UIView!
    @IBOutlet var businessName: UILabel!
    @IBOutlet var busineDescription: UILabel!
    @IBOutlet var countryFlag: UIImageView!
    @IBOutlet var countryName: UILabel!
    @IBOutlet var closeNowLabel: UILabel!
    
    @IBOutlet var saveButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        blurImage()
    }
    
    func blurImage() {
        let darkBlur = UIBlurEffect(style: UIBlurEffectStyle.Dark)
        let blurView = UIVisualEffectView(effect: darkBlur)
        blurView.frame = imageView.bounds
        imageView.addSubview(blurView)
    }

    @IBAction func closeProfileView(){
        profileHeaderDelegate?.backButtonTapped()
    }
    @IBAction func shareBtnTapped(sender: UIButton) {
        print("Share")
    }
    @IBAction func saveBtnTapped(sender: UIButton) {
        print("Save")
        saveButton.selected = !saveButton.selected
        
    }
    

}
