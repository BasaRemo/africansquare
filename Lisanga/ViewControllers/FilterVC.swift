//
//  FilterVC.swift
//  Lisanga
//
//  Created by Professional on 2016-01-09.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit

class FilterVC: UIViewController {

    @IBOutlet var tableView: UITableView!
    @IBOutlet var topView:UIView!
    
    var section0 = ["OPEN NOW", "ALL COUNTRY"]
    var section1 = ["ESSENTIALS", "Restaurants", "Groceries & Markets", "Places To Stay"]
    var section2 = ["SERVICES", "Beauty", "Travel & Transportation",  "Professional Services", "Education", "Organisations"]
    var section3 = ["THINGS TO DO","Shopping", "Bars and Nightlife", "Arts & Culture", "Recreation and Fitness"]
    
    let iconPaths = [
        "", "",
        "","restaurant","shop", "hotel",
        "","beauty","travel","business","school","organisation",
        "", "shopping", "dj", "art", "weightlift"]
    
    var sectionsArray = [[String]]()
    var sections =  [[FilterSectionItem]]()
    
    var selectedItem = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        sectionsArray.append(section0)
        sectionsArray.append(section1)
        sectionsArray.append(section2)
        sectionsArray.append(section3)
        
        var iconPathIdx = 0
        for index in 0...3 {
            var section = [FilterSectionItem]()
            for item in sectionsArray[index] {
                section.append(FilterSectionItem(title: item,mIconPath: iconPaths[iconPathIdx]+"_filled"))
                iconPathIdx++
            }
            sections.append(section)
        }

        tableView.allowsMultipleSelection = true
        tableView.allowsSelectionDuringEditing = true
        
//        self.tableView.backgroundColor = UIColor.lighterGrayColor()
        self.view.backgroundColor = UIColor.lighterGrayColor()
        
        let width = Int(CGRectGetWidth(view.frame))
        let heigth = Int(CGRectGetHeight(view.frame))
        let croppedImage = cropImage(UIImage(named: "africa")!, withWidth: width , andHeigth:heigth)
        self.view.addSubview(UIImageView(image: croppedImage))
        
        let darkBlur = UIBlurEffect(style: UIBlurEffectStyle.Dark)
        let blurView = UIVisualEffectView(effect: darkBlur)
        blurView.frame = self.view.bounds
        self.view.addSubview(blurView)
        
        self.view.bringSubviewToFront(self.tableView)
        self.view.bringSubviewToFront(self.topView)
        
//        self.tableView.setEditing(true, animated: false)
    }

    @IBAction func applyPressed(sender: UIButton) {
        
        let reduced = sections.reduce([], combine: +)
        let filteredArray = reduced.filter({$0.selected == true})
        filteredArray.map({ item in
            print("selected items: \(item.title)")
        })
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func cancelPressed(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}

// Mark - TABLE VIEW
extension FilterVC: UITableViewDataSource {
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch indexPath.section {
            
            case 0:
                if indexPath.row == 0{
                    // Open Now Cell
                    let cell = NSBundle.mainBundle().loadNibNamed("SwitchCell", owner: self, options: nil)[0] as! SwitchCell
                     cell.titleLabel.text = sections[indexPath.section][indexPath.row].title
                
//                    cell.backgroundColor = UIColor.clearColor()
//                    cell.contentView.backgroundColor = UIColor.clearColor()
                    let corners = UIRectCorner.TopLeft.union(UIRectCorner.TopRight)
                    cell.clipsToBounds = true
                    cell.layer.masksToBounds = true
                    cell.layer.cornerRadii = (corners, radius: 6)
                    
                    return cell
                }else{
                    // All country cell
                     let cell = NSBundle.mainBundle().loadNibNamed("MoreCell", owner: self, options: nil)[0] as! MoreCell
                    cell.titleLabel.text = sections[indexPath.section][indexPath.row].title
                    
                    let corners = UIRectCorner.BottomLeft.union(UIRectCorner.BottomRight)
                    cell.clipsToBounds = true
                    cell.layer.masksToBounds = true
                    cell.layer.cornerRadii = (corners, radius: 6)
                    
                    return cell
                }
            
            case 1,2,3:
                if indexPath.row == 0{
                    // Header cell for each section
                    let cell = NSBundle.mainBundle().loadNibNamed("FilterHeaderCell", owner: self, options: nil)[0] as! FilterHeaderCell
                    
                     // Section Header is selected all if all his children are selected
                    let filterItem = sections[indexPath.section][indexPath.row]
                    filterItem.selected = !sections[indexPath.section].contains({!$0.selected})
                    cell.filterItem = filterItem
                    
                    return cell
                }else{
                    //Section Cells
                    let cell = NSBundle.mainBundle().loadNibNamed("FilterCell", owner: self, options: nil)[0] as! FilterCell
//                    cell.titleLabel.text = sections[indexPath.section][indexPath.row].title
                    cell.filterItem = sections[indexPath.section][indexPath.row]
                    
                    // Curve the last cell of each section
                    if (indexPath.row == sections[indexPath.section].count - 1) {
                        
                        let corners = UIRectCorner.BottomLeft.union(UIRectCorner.BottomRight)
                        cell.clipsToBounds = true
                        cell.layer.masksToBounds = true
                        cell.layer.cornerRadii = (corners, radius: 6)
                    }
                    
                    return cell
                }
            
            default:
                let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)
                return cell
        }
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 2
        }else{
            return sections[section].count
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 4
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.section != 0 {
            if indexPath.row == 0 {
                return 47
            }
        }
        return 44
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 3 {
            return 0
        }
        return 15
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let headerView:UIView! = UIView (frame:CGRectMake(0, 0, self.tableView.frame.size.width, 15.0));
        headerView.backgroundColor = UIColor.clearColor()
        
        return headerView;
    }
    
}

extension FilterVC: UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        //Select all item in section
        if indexPath.section != 0 && indexPath.row == 0 {
            let cell =  self.tableView.cellForRowAtIndexPath(indexPath) as! FilterHeaderCell
            
            //If at least 1 item is false => Header is not selected
            if !cell.filterItem.selected{
                for var item in sections[indexPath.section] {
                    item.selected = true
                }
            }else{
                for var item in sections[indexPath.section] {
                    item.selected = false
                }
            }


        }
        
        // Select 1 item in section
        if indexPath.section != 0 && indexPath.row != 0 {
            let cell =  self.tableView.cellForRowAtIndexPath(indexPath) as! FilterCell
            cell.selectItem()
        }

        //Reload section
        let section:NSIndexSet = NSIndexSet(index: indexPath.section)
        tableView.reloadSections(section, withRowAnimation: UITableViewRowAnimation.None)
        
    }
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
}