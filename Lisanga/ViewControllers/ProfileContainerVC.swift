//
//  ProfileContainerVC.swift
//  Lisanga
//
//  Created by Professional on 2015-12-26.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit

class ProfileContainerVC: UIViewController {

    @IBOutlet var header:UIView!
    @IBOutlet var profilContainer:UIView!
    
    private var embeddedProfilVC: ProfileVC!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.embeddedProfilVC.headerView = header
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
//        showShyMenuView()
//        showBLKMenuView()
    }
    override func viewDidAppear(animated: Bool) {
        
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let vc = segue.destinationViewController as? ProfileVC
            where segue.identifier == "profilSegue" {
                
                self.embeddedProfilVC = vc
//                self.embeddedProfilVC.headerView = header
        }
    }

}
