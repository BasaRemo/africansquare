//
//  CategoryMenuVC.swift
//  Lisanga
//
//  Created by Professional on 2015-12-25.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit

protocol CategoryMenuDelegate {
    func didSelectItemAtIndex(menu: CategoryMenuVC,  index: Int, category: String)
}

class CategoryMenuVC: UIViewController {

    @IBOutlet var collectionView:UICollectionView!
    @IBOutlet var menuView:UIView!
    var categoryMenuDelegate: CategoryMenuDelegate?
    var selectedRow:Int = 0
    
    // MARK: - Items
//    private let items = (0..<8 as Range).map {
//        return UIImage(named: "menu_icon_\($0)")!
//    }
//    private let itemsLabel = (0..<7 as Range).map {
//        return "menu\($0)"
//    }
//    private let items = ["discover","restaurant","shopping","shop","beauty","waiter","finances","school","travel"]
//    private let itemsLabel = ["Discover","Restaurant","Shopping","Shop","Beauty","Waiter","Finance","Education","Travel"]
    
    private let items = ["restaurant","shop","beauty","more"]
    private let itemsLabel = ["Restaurant","Markets","Beauty","More"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.collectionView.backgroundColor = THEME_COLOR
        
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
    }

}


extension CategoryMenuVC:UICollectionViewDataSource,UICollectionViewDelegate {
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath)
        
        let imageView: UIImageView = (cell.contentView.viewWithTag(10) as! UIImageView);
        imageView.image = UIImage(named:items[indexPath.row])

        let label:UILabel = (cell.contentView.viewWithTag(11) as! UILabel)
        label.text = itemsLabel[indexPath.row]
        
        imageView.tintColor = UIColor.whiteColor()
        label.textColor = UIColor.whiteColor()
        
        if selectedRow == indexPath.row {
            imageView.image = UIImage(named: "\(items[indexPath.row])_filled")
            imageView.image = imageView.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
            imageView.tintColor = UIColor.orangePositiveColor()
            label.textColor = UIColor.orangePositiveColor()
        }else{
            imageView.image = imageView.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
            imageView.tintColor = UIColor.whiteColor()
            label.textColor = UIColor.whiteColor()
        }

        return cell
    }

    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let cell = collectionView.cellForItemAtIndexPath(indexPath)!
        
        let imageView: UIImageView = (cell.contentView.viewWithTag(10) as! UIImageView);
        imageView.tintColor = UIColor.redColor()
        (cell.contentView.viewWithTag(11) as! UILabel).textColor = UIColor.redColor()
        
        selectedRow = indexPath.row
        collectionView.reloadData()
        //        collectionView.reloadItemsAtIndexPaths([indexPath])
        let category:String = itemsLabel[indexPath.row]
        categoryMenuDelegate?.didSelectItemAtIndex(self, index: indexPath.row,category:category)
    }
    
    override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        collectionView.collectionViewLayout.invalidateLayout()
        collectionView.reloadData()
        collectionView.reloadInputViews()
    }
    
}