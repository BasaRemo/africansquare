//
//  CategoryVC.swift
//  Lisanga
//
//  Created by Professional on 2015-11-04.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit
import Parse

class CategoryVC: UIViewController {
    @IBOutlet var tableView: UITableView!
    var businessArray = [String]()
    var categoryArray = [PFObject]()
    
    @IBOutlet var searchBarButtonItem: UIBarButtonItem!
    var logoImageView   : UIImageView!
    var searchBar = UISearchBar()
    
    enum State {
        case DefaultMode
        case SearchMode
    }
    
    var state: State = .DefaultMode {
        didSet {
            // update notes and search bar whenever State changes
            switch (state) {
            case .DefaultMode:
//                self.navigationController!.setNavigationBarHidden(false, animated: true) //2
                searchBar.resignFirstResponder() // 3
                searchBar.text = ""
                searchBar.showsCancelButton = false
            case .SearchMode:
                let searchText = searchBar.text ?? ""
                searchBar.setShowsCancelButton(true, animated: true) //4
//                self.navigationController!.setNavigationBarHidden(true, animated: true) //6
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        searchBar.delegate = self
        searchBar.searchBarStyle = UISearchBarStyle.Minimal
        searchBarButtonItem = navigationItem.rightBarButtonItem
        searchBar.tintColor = UIColor.whiteColor()
        searchBar.barTintColor = UIColor.whiteColor()
        queryMainCategories()
    }
    
    override func viewWillAppear(animated: Bool) {

        super.viewWillAppear(animated)
        self.navigationController!.setNavigationBarHidden(false, animated: true)
        state = .DefaultMode
    }
    
    func queryMainCategories(){
        
        let query = PFQuery(className:"MainCategory")
        query.findObjectsInBackgroundWithBlock {
            (objects: [PFObject]?, error: NSError?) -> Void in
            
            if error == nil {
                // The find succeeded.
                print("Successfully retrieved \(objects!.count) Categories.")
                // Do something with the found objects
                    for category in objects! {
                        let categoryName:String = category["name"] as! String
                        self.businessArray.append(categoryName)
                        self.categoryArray.append(category)
                        print(categoryName)
                    }
                self.tableView.reloadData()
                
            } else {
                // Log details of the failure
                print("Error: \(error!) \(error!.userInfo)")
            }
        }

    }
    
    func querySecondaryCategories(parentCategoryID:String){
        
        let query = PFQuery(className:"SecondaryCategory")
//        query.equalTo("parent", { __type: "Pointer",
//            className: "MainCategory",
//            objectId: parentCategoryID
//        });
        
        query.whereKey("parent", equalTo: PFObject(withoutDataWithClassName: "MainCategory", objectId: parentCategoryID))

        query.findObjectsInBackgroundWithBlock {
            (objects: [PFObject]?, error: NSError?) -> Void in
            
            if error == nil {
                // The find succeeded.
                print("Successfully retrieved \(objects!.count) Secondary Categories")
                // Do something with the found objects
                self.businessArray.removeAll()
                for category in objects! {
                    let categoryName:String = category["name"] as! String
                    self.businessArray.append(categoryName)
                    print(categoryName)
                }
                self.tableView.reloadData()
                
            } else {
                // Log details of the failure
                print("Error: \(error!) \(error!.userInfo)")
            }
        }
        
    }
    
    

}

extension CategoryVC:UISearchBarDelegate{
    
    @IBAction func searchButtonPressed(sender: AnyObject) {
        showSearchBar()
    }
    
    func showSearchBar() {
        searchBar.alpha = 0
        navigationItem.titleView = searchBar
        navigationItem.setRightBarButtonItem(nil, animated: true)
        UIView.animateWithDuration(0.2, animations: {
            self.searchBar.alpha = 1
            }, completion: { finished in
                self.searchBar.becomeFirstResponder()
        })
    }
    
    func hideSearchBar() {
        navigationItem.setRightBarButtonItem(searchBarButtonItem, animated: true)
        UIView.animateWithDuration(0.2, animations: {
            }, completion: { finished in
                self.searchBar.alpha = 0
                self.navigationItem.titleView?.hidden = true
                self.navigationItem.title = "Category"
                
        })
    }
    
    //MARK: UISearchBarDelegate
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        hideSearchBar()
        state = .DefaultMode
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        state = .SearchMode
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
//        notes = searchNotes(searchText)
    }
    
}
extension CategoryVC: UITableViewDataSource {
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CategoryCell", forIndexPath: indexPath) as! CategoryCell
        
        cell.categoryLabel?.text = businessArray[indexPath.row]
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return businessArray.count ?? 0
    }
    
}

extension CategoryVC: UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let category:PFObject = categoryArray[indexPath.row]{
            
            querySecondaryCategories(category.objectId!)
        }
        
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }
    
}
