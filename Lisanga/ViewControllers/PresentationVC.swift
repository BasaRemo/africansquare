//
//  PresentationVC.swift
//  Lisanga
//
//  Created by Professional on 2016-01-12.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit
import RGCardViewLayout

class PresentationVC: UIViewController {

    @IBOutlet var collectionView:UICollectionView!
    
    private let items = ["barbier","dan1","dan2"]
    private let itemsLabel = ["Restaurant","Markets","Beauty"]
    var selectedRow:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        var insets = self.collectionView.contentInset
//        let value = (self.view.frame.size.width - (self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout).itemSize.width) * 0.5
//        insets.left = value
//        insets.right = value
//        self.collectionView.contentInset = insets
//        print("\(value)")
//        self.collectionView.decelerationRate = UIScrollViewDecelerationRateFast;
        
    }
}

extension PresentationVC:UICollectionViewDataSource,UICollectionViewDelegate {
    
    //RGCardLayout
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return items.count //1
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1 //items.count
    }
    
    // Other layouts
//    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
//        return 1
//    }
//    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return items.count
//    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
//            let cell = tableView.dequeueReusableCellWithIdentifier("PostCell") as! PostTableViewCell
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath)
        
        let imageView: UIImageView = (cell.contentView.viewWithTag(10) as! UIImageView);
        imageView.image = UIImage(named:items[indexPath.section])
        
//        let label:UILabel = (cell.contentView.viewWithTag(11) as! UILabel)
//        label.text = itemsLabel[indexPath.row]
        
        imageView.tintColor = UIColor.blackColor()
//        label.textColor = UIColor.blackColor()
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let cell = collectionView.cellForItemAtIndexPath(indexPath)!
        
        let imageView: UIImageView = (cell.contentView.viewWithTag(10) as! UIImageView);
        imageView.tintColor = UIColor.redColor()
//        (cell.contentView.viewWithTag(11) as! UILabel).textColor = UIColor.redColor()
//
//        selectedRow = indexPath.row
//        collectionView.reloadData()
        //        collectionView.reloadItemsAtIndexPaths([indexPath])
    }
    
}
