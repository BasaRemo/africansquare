//
//  AddBusinessTableVC.swift
//  Lisanga
//
//  Created by Professional on 2015-10-17.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class AddBusinessTableVC: UITableViewController {

    var location: Location? {
        didSet {
            let addressIndexPath = NSIndexPath(forRow: 3, inSection: 0)
            if let cell = tableView.cellForRowAtIndexPath(addressIndexPath) as? OptionCell{
                print("Address:",location?.placemark.subThoroughfare)
                print(" ",location?.placemark.thoroughfare)
                //location?.placemark.region?.description
//                 location.flatMap({ $0.placemark.locality })
                cell.resultLabel.text = (location?.placemark.subThoroughfare)! + " " + (location?.placemark.thoroughfare)! ?? "No location selected"
            }	
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.registerNib(UINib(nibName: "BasicCell", bundle: nil), forCellReuseIdentifier: "BasicCell")
        tableView.registerNib(UINib(nibName: "OptionCell", bundle: nil), forCellReuseIdentifier: "OptionCell")
        tableView.registerNib(UINib(nibName: "DescriptionCell", bundle: nil), forCellReuseIdentifier: "DescriptionCell")
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .Done, target: self, action: "submit:")
    }
    
    /// MARK: Actions
    
    func submit(_: UIBarButtonItem!) {
        
        //Section 0
        let businessNameIndexPath = NSIndexPath(forRow: 0, inSection: 0)
        let phoneIndexPath = NSIndexPath(forRow: 1, inSection: 0)
        let emalIndexPath = NSIndexPath(forRow: 2, inSection: 0)
        let addressIndexPath = NSIndexPath(forRow: 3, inSection: 0)
        
        if let cell = tableView.cellForRowAtIndexPath(businessNameIndexPath) as? BasicCell{
            print("INFO:",cell.textField.text)
        }
        if let cell = tableView.cellForRowAtIndexPath(phoneIndexPath) as? BasicCell{
            print("INFO:",cell.textField.text)
        }
        if let cell = tableView.cellForRowAtIndexPath(emalIndexPath) as? BasicCell{
            print("INFO:",cell.textField.text)
        }
        if let cell = tableView.cellForRowAtIndexPath(addressIndexPath) as? OptionCell{
            print("Option")
        }
        
        //Section 1
        let urlIndexPath = NSIndexPath(forRow: 0, inSection: 1)
        let categoryIndexPath = NSIndexPath(forRow: 1, inSection: 1)
        
        if let cell = tableView.cellForRowAtIndexPath(urlIndexPath) as? BasicCell{
            print("INFO:",cell.textField.text)
        }
        if let cell = tableView.cellForRowAtIndexPath(categoryIndexPath) as? OptionCell{
            print("Option")
        }
        
        
        //Section 2
        let descriptionIndexPath = NSIndexPath(forRow: 0, inSection: 2)

        if let cell = tableView.cellForRowAtIndexPath(descriptionIndexPath) as? DescriptionCell{
            print("Description",cell.descriptionTextView.text)
        }
        
    }

    // MARK: - Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        switch section
        {
        case 0:
            return 7
        case 1:
            return 2
        case 2:
            return 1
        default:
            return 0
        }

    }


    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //let cell = tableView.dequeueReusableCellWithIdentifier("BasicCell", forIndexPath: indexPath) as! BasicCell
        
        let reuseIdentifier = "cell"
        //cell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "cell")
        var cell = UITableViewCell(style: .Default, reuseIdentifier: reuseIdentifier)

        if (indexPath.section == 0) {
            print("SECTION 0)")
            switch indexPath.row
            {
                case 0:
                    let basicCell = tableView.dequeueReusableCellWithIdentifier("BasicCell", forIndexPath: indexPath) as! BasicCell
                    basicCell.titleLabel.text = "Business Name: "
                    basicCell.textField.placeholder = "e.g. Ela Jambo"
                    return basicCell
                
                case 1:
                    let basicCell = tableView.dequeueReusableCellWithIdentifier("BasicCell", forIndexPath: indexPath) as! BasicCell
                    basicCell.titleLabel.text = "Phone:  "
                    basicCell.textField.placeholder = "e.g. 514 254 8934"
                    return basicCell
                case 2:
                    let basicCell = tableView.dequeueReusableCellWithIdentifier("BasicCell", forIndexPath: indexPath) as! BasicCell
                    basicCell.titleLabel.text = "Email: "
                    basicCell.textField.placeholder = "e.g. ElaJambo@gmail.com"
                    return basicCell
                case 3:
                    let optionCell = tableView.dequeueReusableCellWithIdentifier("OptionCell", forIndexPath: indexPath) as! OptionCell
                    optionCell.titleLabel.text = "Address:"
//                    optionCell.resultLabel.text = "No location selected"
                    return optionCell
                case 4:
                    let basicCell = tableView.dequeueReusableCellWithIdentifier("BasicCell", forIndexPath: indexPath) as! BasicCell
                    basicCell.titleLabel.text = "Details: "
                    basicCell.textField.placeholder = "e.g. African Language school"
                    return basicCell
                case 5:
                    let optionCell = tableView.dequeueReusableCellWithIdentifier("OptionCell", forIndexPath: indexPath) as! OptionCell
                    optionCell.titleLabel.text = "Africa:"
                    //                    optionCell.resultLabel.text = "No location selected"
                    return optionCell
                case 6:
                    let optionCell = tableView.dequeueReusableCellWithIdentifier("OptionCell", forIndexPath: indexPath) as! OptionCell
                    optionCell.titleLabel.text = "Image:"
                    //                    optionCell.resultLabel.text = "No location selected"
                    return optionCell
                
                default: break
            }
            
        }else if (indexPath.section == 1){
            print("SECTION 1)")
            switch indexPath.row
            {
                case 0:
                    let basicCell = tableView.dequeueReusableCellWithIdentifier("BasicCell", forIndexPath: indexPath) as! BasicCell
                    basicCell.titleLabel.text = "URL:  "
                    basicCell.textField.placeholder = "e.g. www.elaJambo.com"
                    return basicCell
                case 1:
                    let optionCell = tableView.dequeueReusableCellWithIdentifier("OptionCell", forIndexPath: indexPath) as! OptionCell
                     optionCell.titleLabel.text = "Category"
                    return optionCell
                default: break
            }
            
            
        }else if (indexPath.section == 2){
            print("SECTION 2)")
            switch indexPath.row
            {
                case 0:
                    let descriptionCell = tableView.dequeueReusableCellWithIdentifier("DescriptionCell", forIndexPath: indexPath) as! DescriptionCell
                    return descriptionCell
                default: break
            }
            
        }

        return cell
    }

}
extension AddBusinessTableVC {
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if (indexPath.section == 0) {
            switch indexPath.row
            {
            case 3:
                SelectAddress()
            default: break
            }
            
        }else if (indexPath.section == 1){
            switch indexPath.row
            {
            case 1:break
            default: break
            }
        }
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }
        
    func SelectAddress() {
        
        let locationPicker = LocationPickerViewController()
        
        // you can optionally set initial location
        let coordinates = CLLocationCoordinate2D(latitude: 43.25, longitude: 76.95)
        let initialLocation = Location(name: "Address", location: nil,
            placemark: MKPlacemark(coordinate: coordinates, addressDictionary: [:]))
        locationPicker.location = initialLocation
        
        // optional region distance to be used for creation region when user selects place from search results (defaults to 600)
        locationPicker.resultRegionDistance = 500
        
        locationPicker.completion = { location in
            // do some awesome stuff with location
            self.location = location
            print("location:", location)
        }
        
        navigationController?.pushViewController(locationPicker, animated: true)
        
    }
    
}