//
//  ProfileVC.swift
//  Lisanga
//
//  Created by Professional on 2015-12-22.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit
import MXSegmentedPager
import SABlurImageView
import PureLayout
import DOFavoriteButton

class ProfileVC: MXSegmentedPagerController {

    var headerView:ProfileView!
    var business:Business?
    var infoDetailsTVC: InfoTVC?
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if let _ = self.infoDetailsTVC{
            self.infoDetailsTVC!.business = self.business
        }
        
        if let _ = self.navigationController {
//            self.navigationController!.setNavigationBarHidden(true, animated: true)
//            self.navigationController?.navigationBar.translucent = true
//            self.navigationController?.navigationBar.backgroundColor = UIColor.clearColor()
        }
        setupHeaderView()
    }
    
    override func viewDidDisappear(animated: Bool) {
//        self.segmentedPager.segmentedControlPosition = MXSegmentedControlPosition.Top
//        self.segmentedPager.updateParallaxHeaderViewHeight(300)

    }
    func setupHeaderView(){
        // Parallax Header
        var uiview = NSBundle.mainBundle().loadNibNamed(ProfileViewCellIdentifier, owner: self, options: nil)[0] as? ProfileView
        headerView = uiview
        if let business = self.business {
            headerView.nameLabel.text = business.name ?? ""
            headerView.descriptionLabel.text = business.categoryList[0] ?? "Description"
            headerView.categoryLabel.text = business.categoryList[0] as String ?? "Category"
            if let image = business.businessImage {
                headerView.backgroundColor = UIColor(patternImage: image)
                headerView.profileImage.image = image
            }
        }
        
        setupSegmentPager()
    }

    func setupSegmentPager(){
        
//        self.segmentedPager.backgroundColor = UIColor(patternImage: UIImage(named: "success-baby")!)
        self.segmentedPager.parallaxHeader.view = headerView
        self.segmentedPager.parallaxHeader.mode = MXParallaxHeaderMode.Fill
        self.segmentedPager.parallaxHeader.height = 230;
        self.segmentedPager.parallaxHeader.minimumHeight = 130;
        
        let button = DOFavoriteButton(frame: CGRectMake(self.segmentedPager.segmentedControl.frame.origin.x + 170, -60, 120, 120), image: UIImage(named: "heart"))
        button.imageColorOn = UIColor.redColor()
        button.circleColor = UIColor.redColor()
        button.lineColor = UIColor.redColor()
        button.addTarget(self, action: Selector("tapped:"), forControlEvents: .TouchUpInside)
        //        self.segmentedPager.segmentedControl.addSubview(button)
        
        // Segmented Control customization
        self.segmentedPager.segmentedControl.backgroundColor = UIColor.greenColor()
        self.segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
        self.segmentedPager.segmentedControl.backgroundColor = UIColor.whiteColor()
        self.segmentedPager.segmentedControl.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.lightGrayColor(), NSBackgroundColorAttributeName: UIColor.clearColor(), NSFontAttributeName: UIFont.systemFontOfSize(13)]
        self.segmentedPager.segmentedControl.selectedTitleTextAttributes = [NSForegroundColorAttributeName : UIColor.orangeColor()]
        self.segmentedPager.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe
        self.segmentedPager.segmentedControl.selectionIndicatorColor = UIColor.orangeColor()
        self.segmentedPager.segmentedControl.selectionIndicatorHeight = 3
//        self.segmentedPager.segmentedControl.selectionIndicatorEdgeInsets = UIEdgeInsetsMake(100, 50, 0, -20)
    }
    
    func tapped(sender: DOFavoriteButton) {
        if sender.selected {
            // deselect
            sender.deselect()
        } else {
            // select with animation
            sender.select()
        }
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let vc = segue.destinationViewController as? InfoTVC
            where segue.identifier == "mx_page_0" {
                self.infoDetailsTVC = vc
                self.infoDetailsTVC!.business = self.business
        }
    }
}

extension ProfileVC{
    override func segmentedPager(segmentedPager: MXSegmentedPager, titleForSectionAtIndex index: Int) -> String {
        return ["Info","Review"][index];
    }
    
    override func segmentedPager(segmentedPager: MXSegmentedPager, didScrollWithParallaxHeader parallaxHeader: MXParallaxHeader) {
//                NSLog("progress %li", parallaxHeader.progress)
    }
}

extension ProfileVC:ProfileHeaderDelegate{
    func backButtonTapped(){
//        self.dismissViewControllerAnimated(true, completion: nil)
        self.navigationController?.popViewControllerAnimated(true)
    }
}
