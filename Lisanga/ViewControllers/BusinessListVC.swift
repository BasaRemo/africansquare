//
//  BusinessListVC.swift
//  Lisanga
//
//  Created by Professional on 2015-10-17.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit
import Parse
import Persei
import ZFDragableModalTransition
import TLYShyNavBar
import BLKFlexibleHeightBar
import SVProgressHUD
import MGSwipeTableCell

var FILTER_CATEGORY = ""

class BusinessListVC: UIViewController {

    @IBOutlet var tableView: UITableView!
    @IBOutlet var menuContainer: UIView!
    
    var businessArray = ["Ela Jambo", "Mama Africa", "OPCC", "Steph Odia","Congo Mikili", "Jeune Afrique", "Gentlemen 1st", "PrimeTime Training","Ela Jambo", "Mama Africa", "OPCC", "Steph Odia","Congo Mikili", "Jeune Afrique", "Gentlemen 1st", "PrimeTime Training","Ela Jambo", "Mama Africa", "OPCC", "Steph Odia","Congo Mikili", "Jeune Afrique", "Gentlemen 1st", "PrimeTime Training","Ela Jambo", "Mama Africa", "OPCC", "Steph Odia","Congo Mikili", "Jeune Afrique", "Gentlemen 1st", "PrimeTime Training"]
    var businessImages = ["01","02","03","04","05","06","07","08","09","01","02","03","04","05","06","07","01","02","03","04","05","06","07","08","09","01","02","03","04","05","06","07"]
    
    var modalVC:ProfileVC?
    var menuVC:CategoryMenuVC?
    var filterVC:FilterVC?
    
    var currentBusiness:Business?
    var businessList:[Business]?
//    var filteredArray = [Business]()
    var filteredArray: [Business] = []
    private var menu: MenuView!
    var animator:ZFModalTransitionAnimator!
    
    // Timeline Component Protocol
    let defaultRange = 0...4
    let additionalRangeSize = 3
    var timelineComponent: TimelineComponent<Business, BusinessListVC>!
    
    var mainSection:Int = 0
    var delegateSplitter: BLKDelegateSplitter?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        SVProgressHUD.show()
        self.timelineComponent = TimelineComponent(target: self)
//        searchUserInParse()

        modalVC = self.storyboard?.instantiateViewControllerWithIdentifier("ProfileVC") as? ProfileVC
        menuVC = self.storyboard?.instantiateViewControllerWithIdentifier("CategoryMenuVC") as? CategoryMenuVC
        filterVC  = self.storyboard?.instantiateViewControllerWithIdentifier("FilterVC") as? FilterVC
        menuVC!.categoryMenuDelegate = self
        
        self.animator = ZFModalTransitionAnimator(modalViewController: modalVC)
        self.animator.dragable = true
        self.animator.bounces = false
        self.animator.behindViewAlpha = 0.5
        self.animator.behindViewScale = 1.0
        self.animator.transitionDuration = 0.7
        self.animator.direction = ZFModalTransitonDirection.Right
        modalVC!.transitioningDelegate = self.animator
        modalVC!.modalPresentationStyle = UIModalPresentationStyle.Custom
        
        OBSERVE_SearchBarChange(self)
         timelineComponent.loadInitialIfRequired()
        
//        let width = Int(CGRectGetWidth(view.frame))
//        let heigth = Int(CGRectGetHeight(view.frame))
//        let croppedImage = cropImage(UIImage(named: "africa")!, withWidth: width , andHeigth:heigth)
//        self.view.addSubview(UIImageView(image: croppedImage))
//        
//        let darkBlur = UIBlurEffect(style: UIBlurEffectStyle.Dark)
//        let blurView = UIVisualEffectView(effect: darkBlur)
//        blurView.frame = self.view.bounds
//        self.view.addSubview(blurView)
//        self.view.bringSubviewToFront(self.tableView)
    
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
//        showBLKMenuView()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(animated)

    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        
          SVProgressHUD.dismiss()
        showShyMenuView()
//        showBLKMenuView()
    }

    func ReceivedSearchBarNotification(notification: NSNotification){
        
        var isHidden: Bool = (notification.object as? Bool)!
        if !isHidden {
            self.shyNavBarManager.extensionView.hidden = true
            self.timelineComponent.removeAllContent()
            
        }else{
             self.shyNavBarManager.extensionView.hidden = false
            self.timelineComponent.loadInitialIfRequired()
        }
    }
    
    func ShowProfileView() {
        
        //    self.applyBlurEffect()
        //self.animator.setContentScrollView(modalVC!.tableView)
        modalVC!.business = self.currentBusiness
//          presentViewController(modalVC!, animated: true, completion: nil)
        self.navigationController!.pushViewController(modalVC!, animated: true)
        self.tableView.deselectRowAtIndexPath( self.tableView.indexPathForSelectedRow!, animated: false)
    }
    
    // Filter the received Array
    func filterArray(posts:[Business]) -> [Business]{
        
        let filterPosts = posts.filter() {
            let business:Business = $0
            if let type = business.businessType! as? String {
                return type.rangeOfString(FILTER_CATEGORY, options: .CaseInsensitiveSearch) != nil
            } else {
                return false
            }
        }
        return filterPosts
        //      print("Filter count: \(self.filteredArray.count)")
    }
    
    //Hide tab bar
//    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
//        if scrollView.panGestureRecognizer.translationInView(scrollView).y < 0{
//            changeTabBar(true, animated: true)
//        }
//        else{
//            changeTabBar(false, animated: true)
//        }
//    }
//    
//    func changeTabBar(hidden:Bool, animated: Bool){
//        var tabBar = self.tabBarController?.tabBar
//        if tabBar!.hidden == hidden{ return }
//        let frame = tabBar?.frame
//        let offset = (hidden ? (frame?.size.height)! : -(frame?.size.height)!)
//        let duration:NSTimeInterval = (animated ? 0.5 : 0.0)
//        tabBar?.hidden = false
//        if frame != nil
//        {
//            UIView.animateWithDuration(duration,
//                animations: {tabBar!.frame = CGRectOffset(frame!, 0, offset)},
//                completion: {
//                    print($0)
//                    if $0 {tabBar?.hidden = hidden}
//            })
//        }
//    }
//    
}

// Mark - TABLE VIEW
extension BusinessListVC: UITableViewDataSource {
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("BusinessCell", forIndexPath: indexPath) as! BusinessCell
        
//        if indexPath.section == mainSection {
//            if self.filteredArray.count > 0{
//                if let business:Business = filteredArray[indexPath.row] {
//                    cell.business = business
//                }
//            }else{
//                if let business:Business = timelineComponent.content[indexPath.row] {
//                    cell.business = business
//                }
//            }
//        }
        
        if self.filteredArray.count > 0{
            if let business:Business = filteredArray[indexPath.section] {
                cell.business = business
            }
        }else{
            if let business:Business = timelineComponent.content[indexPath.section] {
                cell.business = business
            }
        }
        
        cell.rightButtons = [MGSwipeButton(title: "Map",backgroundColor: UIColor.primaryGrayColor(),padding: 18, callback: {
            (sender: MGSwipeTableCell!) -> Bool in
            print("Convenience callback for swipe buttons!")
            return true
        })]
        cell.rightSwipeSettings.transition = MGSwipeTransition.Border

        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
//        if section == 0{
//            return 0  //Always 0
//        }
//        //Recommandation Section // Should be the number of recommandations we want Default: 1
//        else if section == 1 {
//            return 1 //1
//            
//        }else
//            
//        if section == mainSection {
//            
//            if FILTER_CATEGORY != "" {
//                if self.filteredArray.count > 0{
//                    return self.filteredArray.count
//                }
//            }
//            else{
//                return self.timelineComponent.content.count ?? 0
//            }
//            return self.timelineComponent.content.count ?? 0
//        }
        return 1
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.timelineComponent.content.count ?? 0 
    }
    
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 8
    }
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor.clearColor()
        return view
    }
    
//    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
////        if section == mainSection {
////            return 1 // Default: 25
////        }
//        return 10
//    }
//
//    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        //Used when we activate recommandations
//        let  headerCell:HeaderCell = NSBundle.mainBundle().loadNibNamed("HeaderCell", owner: self, options: nil)[0] as! HeaderCell
//        if section == 0 {
//            headerCell.titleLabel.text = "Recommandations"
//            return headerCell;
//        }else
//            if section == 1{
//            headerCell.titleLabel.text = "More"
//            return headerCell;
//        }
//        return nil
//    }
    
    
}

extension BusinessListVC: UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
//        if indexPath.section == mainSection {
//            self.currentBusiness = timelineComponent.content[indexPath.row]
//            print("Selected: \(currentBusiness?.name) ")
//            self.ShowProfileView()
//        }
        self.currentBusiness = timelineComponent.content[indexPath.section]
        print("Selected: \(currentBusiness?.name) ")
        self.ShowProfileView()
        
    }
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
//        if indexPath.section == mainSection {
//            timelineComponent.targetWillDisplayEntry(indexPath.row)
//        }
    }

}

// MARK: TimelineComponentTarget implementation
extension BusinessListVC: TimelineComponentTarget{
    
    func loadInRange(range: Range<Int>, completionBlock: ([Business]?) -> Void) {
        
        ParseHelper.sharedInstance.getParseBusiness(range, distanceFilter: DEFAULT_DISTANCE) {
            (results: [PFObject]?, error: NSError?) -> Void in
            if error == nil {

                let posts = results as? [Business] ?? []
                if FILTER_CATEGORY != "" {
                    self.filteredArray = self.filteredArray + self.filterArray(posts)
                }
               
                completionBlock(posts)
                
            } else {
                // Log details of the failure
                print("Error: \(error!) \(error!.userInfo)")
            }
        }
    }
    
    func reload(additionalRange: Range<Int>) {
        // TODO- Use tableView.insertRowsAtIndexPaths instead of tableview.reloadData() -- See commit 326fa51 of this function
        tableView.reloadData()
        
    }
}

// Mark: MENU implementation
extension BusinessListVC: CategoryMenuDelegate {
    
    func didSelectItemAtIndex(menu: CategoryMenuVC,  index: Int, category:String){
        print("\(category)")
        if category == "More" {
            self.presentViewController(filterVC!, animated: true, completion: nil)
        }else{
            FILTER_CATEGORY = category
            if category == "Discover"{
                FILTER_CATEGORY = ""
                self.filteredArray.removeAll()
                self.tableView.reloadData()
            }
            
            // Filter data with the selected category
            if FILTER_CATEGORY != "" {
                let posts = timelineComponent.content as? [Business] ?? []
                self.filteredArray = self.filterArray(posts)
                self.tableView.reloadData()
                print("Filter count: \(self.filteredArray.count)")
            }else{
                
                self.filteredArray.removeAll()
                self.tableView.reloadData()
            }
            
            let transition = CircularRevealTransition(layer: self.view.layer, center: CGPointMake(self.view.frame.size.width/2 - 100, 30))
            transition.start()
        }
    }
    
    // Mark - Custom Menu Function
    func showShyMenuView(){
        let view = UIView(frame: CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 60))
//        view.backgroundColor = UIColor.primaryBlueColor()
        if let menuItemView = menuVC?.view {
            menuVC!.view.frame = view.bounds;
            view.addSubview( menuItemView)
        }
        
//        let view = NSBundle.mainBundle().loadNibNamed("FilterMenuView", owner: self, options: nil)[0] as! FilterMenuView
//        view.delegate = self
        
        self.shyNavBarManager.scrollView = self.tableView;
        self.shyNavBarManager.extensionView = view
        self.shyNavBarManager.stickyNavigationBar = true
        self.shyNavBarManager.stickyExtensionView = false
        self.shyNavBarManager.fadeBehavior = .Navbar
        self.shyNavBarManager.expansionResistance = 200
        self.shyNavBarManager.contractionResistance = 0
    }
    func showBLKMenuView(){
        let bar = BLKFlexibleHeightBar(frame: CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 70))
        bar.minimumBarHeight = 10.0
        bar.backgroundColor = UIColor.clearColor()
        if let menuItemView = menuVC?.view {
            menuVC!.view.frame = bar.bounds;
            bar.addSubview( menuItemView)
        }
        self.view.addSubview(bar)
        bar.behaviorDefiner = SquareCashStyleBehaviorDefiner()
        self.tableView.delegate = bar.behaviorDefiner as? UITableViewDelegate
        
        bar.behaviorDefiner.addSnappingPositionProgress(0.0, forProgressRangeStart:0.0, end:0.5)
        bar.behaviorDefiner.addSnappingPositionProgress(1.0, forProgressRangeStart:0.5, end:1.0)
        
        self.delegateSplitter = BLKDelegateSplitter(firstDelegate: bar.behaviorDefiner, secondDelegate: self)
        
    }
}

extension BusinessListVC:FilterViewDelegate{
    func filterBtnPressed(filter:FilterMenuView){
        self.presentViewController(filterVC!, animated: true, completion: nil)
    }
}