//
//  InfoTVC.swift
//  Lisanga
//
//  Created by Professional on 2015-12-23.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit
import TagListView
import MessageUI
import AudioToolbox

//import Smooch


protocol ProfilScrollDelegate {
    func scrollViewDidScroll(scrollView: UIScrollView,velocity: CGPoint)
}

class InfoTVC: UITableViewController {
    
    @IBOutlet var headerView: UIView!
    @IBOutlet var tagListView: TagListView!
    @IBOutlet var phoneNumber: UILabel!
    @IBOutlet var websiteUrl: UILabel!
    @IBOutlet var email: UILabel!
    @IBOutlet var address: UILabel!
    @IBOutlet var distance: UILabel!
    
    @IBOutlet var businessHoursView: UIView!
    var globalMail:MFMailComposeViewController?
    
    var business:Business?
    var menuVC:CategoryMenuVC?
    var profilScrollDelegate:ProfilScrollDelegate?
    var webViewVC:WebViewVC?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Code to hide the separtor between navbar and the view
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        let px = 1 / UIScreen.mainScreen().scale
        let frame = CGRectMake(0, 0, self.tableView.frame.size.width, px)
        let line: UIView = UIView(frame: frame)
        self.tableView.tableHeaderView = line
        line.backgroundColor = self.tableView.separatorColor
        
        tagListView.delegate = self
        webViewVC = self.storyboard?.instantiateViewControllerWithIdentifier("WebViewVC") as? WebViewVC
        
        setupViewContent()
        
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        tagListView.removeAllTags()
        setupViewContent()
    }
    override func viewDidDisappear(animated: Bool) {
        self.tableView.setContentOffset(CGPointZero, animated:false)
    }
    func setupViewContent(){
        
        if let business = self.business{
//            print("BUSINESS: \(business)")
            self.phoneNumber.text = business.phoneNumber ?? "Unavailable"
            self.websiteUrl.text = business.websiteUrl ?? "Unavailable"
            self.email.text = business.email ?? "Unavailable"
            self.address.text = business.address ?? "Unavailable"
            
            //Set categoriesView
            for category: String in business.categoryList {
                tagListView.addTag(category)
            }
            
            //Set Business Hours
            for (index,hours) in business.businessHoursList.enumerate() {
                let hourLabel:UILabel = (businessHoursView.viewWithTag(index + 1) as! UILabel)
                hourLabel.text = hours
                print("hours: \(hours)")
                
            }
        }
    }
    override func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
    {
        let header = view as! UITableViewHeaderFooterView
//        header.textLabel?.font = UIFont(name: "System", size: 12)!
        header.textLabel?.textColor = UIColor.lightGrayColor()
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        switch indexPath.row
        {
            case 0,1,2,3,4:
                showOptionList(indexPath.row)
            default:
                print("row \(indexPath.row)")

        }
        self.tableView.deselectRowAtIndexPath( self.tableView.indexPathForSelectedRow!, animated: false)
    }
    
    func alertToSave(){
        
        let alertController = UIAlertController(title: "Save Playlist", message:
            "", preferredStyle: UIAlertControllerStyle.Alert)
        
        alertController.addTextFieldWithConfigurationHandler { (textField) in
            textField.placeholder = "New Playlist Name"
        }
        alertController.addAction(UIAlertAction(title: "Save", style: UIAlertActionStyle.Default,handler: { (action) -> Void in
            //                let textField = alertController.textFields![0] as UITextField
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default,handler: { (action) -> Void in
            self.dismissViewControllerAnimated(true, completion: nil)
        }))
        self.presentViewController(alertController, animated: true){
        }
        print("save case")
    }
    
    func showOptionList(index:Int){
        
        
        var alert: UIAlertController!
        var firstAction: UIAlertAction!
        var secondAction: UIAlertAction!
        var alertTitle = "Call"
        var actionTitle = "\(business!.phoneNumber!)"
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: {(alert: UIAlertAction!) in
            print("cancel")
        })
        
        switch index
        {
            case 0:
                alertTitle = "Call"
                actionTitle = "\(business!.phoneNumber!)"
                alert = UIAlertController(title: "\(alertTitle) this business", message: "", preferredStyle: .ActionSheet)
                firstAction = UIAlertAction(title: actionTitle, style: .Default) { (alert: UIAlertAction!) -> Void in
                    self.callNumber(self.business!.phoneNumber!)
                }
                alert.addAction(firstAction)
                alert.addAction(cancelAction)
                presentViewController(alert, animated: true, completion:nil)
                
            case 1:
                alertTitle = "\(business!.address!)"
                alert = UIAlertController(title: "\(alertTitle)", message: "", preferredStyle: .ActionSheet)
                firstAction = UIAlertAction(title: "Copy to clipboard", style: .Default) { (alert: UIAlertAction!) -> Void in
                    UIPasteboard.generalPasteboard().string = "\(self.business!.address!)"
                    //Make system sound
    //                self.playSystemSound()
                    
                }
                secondAction = UIAlertAction(title: "Show in Map", style: .Default) { (alert: UIAlertAction!) -> Void in
                    
                }
                alert.addAction(secondAction)
                alert.addAction(firstAction)
                alert.addAction(cancelAction)
                presentViewController(alert, animated: true, completion:nil)
                
            case 2:
                print("business hours");
            
            case 3:
                alertTitle = "Visit"
                actionTitle = "\(business!.websiteUrl!)"
                alert = UIAlertController(title: "\(alertTitle) our website", message: "", preferredStyle: .ActionSheet)
                firstAction = UIAlertAction(title: actionTitle, style: .Default) { (alert: UIAlertAction!) -> Void in
                    self.navigationController?.pushViewController(self.webViewVC!, animated: true)
                }
                alert.addAction(firstAction)
                alert.addAction(cancelAction)
                presentViewController(alert, animated: true, completion:nil)
                
            case 4:
                alertTitle = "Email"
                actionTitle = "\(business!.email!)"
                alert = UIAlertController(title: "\(alertTitle) this business", message: "", preferredStyle: .ActionSheet)
                firstAction = UIAlertAction(title: actionTitle, style: .Default) { (alert: UIAlertAction!) -> Void in
                    //self.sendEmailButtonTapped()
                    Smooch.show()
                }
                alert.addAction(firstAction)
                alert.addAction(cancelAction)
                presentViewController(alert, animated: true, completion:nil)
            
            default:
                break
        }
        
    }
    
    //System Keyboard Tock Sound
    func playSystemSound() {
        
        let filePath = NSBundle.mainBundle().pathForResource("Tock", ofType: "caf")
        let fileURL = NSURL(fileURLWithPath: filePath!)
        var soundID:SystemSoundID = 0
        AudioServicesCreateSystemSoundID(fileURL, &soundID)
        AudioServicesPlaySystemSound(soundID)
        
    }
    
    private func callNumber(phoneNb:String) {
        
        if let phoneCallURL:NSURL = NSURL(string: "tel://\(phoneNb.numbersOnly)") {
            let application:UIApplication = UIApplication.sharedApplication()
            if (application.canOpenURL(phoneCallURL)) {
                application.openURL(phoneCallURL);
            }
        }
    }
    
    override func scrollViewWillEndDragging(scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
    
//        //Scroll up
//        if (velocity.y > 0){
//            //            prin("up")
//        }
//        // Scroll down
//        if (velocity.y < 0){
//            //            prin("down")
//        }
//        profilScrollDelegate?.scrollViewDidScroll(scrollView, velocity: velocity)
    }

}

extension InfoTVC:TagListViewDelegate{
    func tagPressed(title: String, tagView: TagView, sender: TagListView) {
        print("Tag pressed: \(title), \(sender)")
        
        tagView.selected = !tagView.selected
    }
}

extension InfoTVC:MFMailComposeViewControllerDelegate {
    
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func sendEmailButtonTapped() {
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.presentViewController(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(["someone@somewhere.com"])
        mailComposerVC.setSubject("Sending you an in-app e-mail...")
        mailComposerVC.setMessageBody("Sending e-mail in-app is not so bad!", isHTML: false)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }
    
}
