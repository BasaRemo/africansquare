//
//  ParseHelper.swift
//  OfficeHours
//
//  Created by Eliel Gordon on 12/3/15.
//  Copyright © 2015 Saltar Group. All rights reserved.
//
//
import Parse

let DEFAULT_RANGE = 0...3
let ADDITIONAL_RANGE_SIZE = 2
let DEFAULT_DISTANCE = 20000.0

class ParseHelper: NSObject {
    
    //Singleton
    static let sharedInstance = ParseHelper()
    private override init() {}
    
    //Classes
    let ParseCategory = "Category"
    // Connection Relation
    let ParseBusinessClass = "Business"
    let ParseFromBusiness = "fromBusiness"
    let ParseToMainCategory = "toMainCategory"

    
    // Business properties
    let ParseBusinessLocation = "location"
    let username = "username"
    let ParseCategoryList = "categoryList"
    
     let ParseInspiration = "Inspiration"
    /**
    Fetches nearby businesses according to the distance filter and range
    
    - parameter range:           The number of results to display
    - parameter distanceFilter:  The radius of reach for the search
    - parameter completionBlock: Returns Businesses that are around a user
    */
    
     func getParseBusiness (range: Range<Int>, distanceFilter: Double, completionBlock: PFQueryArrayResultBlock) {
        
        let query = PFQuery(className: ParseBusinessClass)
        PFGeoPoint.geoPointForCurrentLocationInBackground {
            (geoPoint, error) -> Void in
            guard let geoPoint = geoPoint else {return}
            
//            query.whereKey(self.ParseBusinessLocation, nearGeoPoint: geoPoint, withinMiles: distanceFilter)
//                .whereKey(self.ParseIsMentor, equalTo: true)
            
            // Find objects where the array in arrayKey contains each of the elements 2, 3, and 4.
//            query.whereKey("CategoryList", containsAllObjectsInArray:[2, 3, 4])
//            query.whereKey("CategoryList", equalTo: 2)

            query.includeKey(self.ParseCategoryList)
            query.skip = range.startIndex
            query.limit = range.endIndex - range.startIndex
            
             query.findObjectsInBackgroundWithBlock(completionBlock)
        }
    }
    
    // MARK: Category
    
    /**
    Fetches all categories associate to the business
    
    - parameter user: The user who's followees you want to retrive
    - parameter completionBlock: The completion block that is called when the query completes
    */
    func getCategoriesForBusiness(business: Business, completionBlock: PFQueryArrayResultBlock) {
        let query = PFQuery(className: ParseCategory)
        
        query.whereKey(ParseFromBusiness, equalTo:business)
        query.includeKey(ParseToMainCategory)
        query.findObjectsInBackgroundWithBlock(completionBlock)
    }
    
    /**
     Fetches Inspiration entrepreneurs
     
     - parameter range:           The number of results to display
     - parameter distanceFilter:  The radius of reach for the search
     - parameter completionBlock: Returns Businesses that are around a user
     */
    
    func getParseInspirationEntrepreneurs (range: Range<Int>, completionBlock: PFQueryArrayResultBlock) {
        
        let query = PFQuery(className: ParseInspiration)
//            query.whereKey(self.ParseBusinessLocation, nearGeoPoint: geoPoint, withinMiles: distanceFilter)
//                .whereKey(self.ParseIsMentor, equalTo: true)
        
// Find objects where the array in arrayKey contains each of the elements 2, 3, and 4.
//            query.whereKey("CategoryList", containsAllObjectsInArray:[2, 3, 4])
//            query.whereKey("CategoryList", equalTo: 2)
        
        query.skip = range.startIndex
        query.limit = range.endIndex - range.startIndex
        
        query.findObjectsInBackgroundWithBlock(completionBlock)
    }
    
}
