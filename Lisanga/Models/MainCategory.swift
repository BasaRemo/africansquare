//
//  MainCategory.swift
//  Lisanga
//
//  Created by Professional on 2015-10-20.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import Foundation
import Parse

class MainCategory: PFObject, PFSubclassing {
    
    @NSManaged var name: String?
    //MARK: PFSubclassing Protocol
    static func parseClassName() -> String {
        return "MainCategory"
    }
    
    override init () {
        super.init()
    }
    
    func addBusiness() {
        self.saveInBackgroundWithBlock {
            (success: Bool, error: NSError?) -> Void in
            if (success) {
                // The score key has been incremented
                print("Success adding Business to Parse")
            } else {
                // There was a problem, check error.description
                print("Error adding Business to Parse")
            }
        }
    }
    
    override class func initialize() {
        var onceToken : dispatch_once_t = 0;
        dispatch_once(&onceToken) {
            // inform Parse about this subclass
            self.registerSubclass()
        }
    }
}