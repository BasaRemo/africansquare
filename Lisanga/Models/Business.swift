//
//  Business.swift
//  Lisanga
//
//  Created by Professional on 2015-10-20.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import Foundation
import Parse
import Bond

enum BusinessType {
    case Sushi, Japanese, Asian
}

class Business: PFObject, PFSubclassing {
    
    @NSManaged var name: String?
    @NSManaged var phoneNumber: String?
    @NSManaged var email: String?
    @NSManaged var websiteUrl: String?
    @NSManaged var facebookUrl: String?
    
    @NSManaged var address: String?
    @NSManaged var city: String?
    @NSManaged var state: String?
    @NSManaged var country: String?

    @NSManaged var shortDescription: String?
    @NSManaged var slogan: String?
    
    @NSManaged var africanCountry: String?
    @NSManaged var africaIsoCode: String?
    
    @NSManaged var businessType: String?
    @NSManaged var businessHours: String?
    
    @NSManaged var imageFile: PFFile?
    @NSManaged var owner: PFUser?
    @NSManaged var location: PFGeoPoint?

    
    var imageDownloaded:Bool = false
    var image : Observable<UIImage?> = Observable<UIImage?>(nil)
    var businessImage:UIImage?
    var categories = [Category]()
    var categoryList = [String]()
    var businessHoursList = [String]()
//     static var imageCache: NSCacheSwift<String, UIImage>!
    //MARK: PFSubclassing Protocol
    static func parseClassName() -> String {
        return "Business"
    }
    
    override init () {
        super.init()
        
    }
    
//    func addBusiness() {
//        self.saveInBackgroundWithBlock {
//            (success: Bool, error: NSError?) -> Void in
//            if (success) {
//                // The score key has been incremented
//                print("Success adding Business to Parse")
//            } else {
//                // There was a problem, check error.description
//                print("Error adding Business to Parse")
//            }
//        }
//    }
//    
    override class func initialize() {
        var onceToken : dispatch_once_t = 0;
        dispatch_once(&onceToken) {
            // inform Parse about this subclass
            self.registerSubclass()
        }
    }

    func downloadImage() {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            // do your task
            self.imageFile?.getDataInBackgroundWithBlock { (data: NSData?, error: NSError?) -> Void in
                if let error = error {
                    print("error downloading image")
                }
                
                if let data = data {
                    let image = UIImage(data: data, scale:1.0)!
                    self.image.value = image
                    self.businessImage = image
//                    print("image retrieved")
                }
            }
            dispatch_async(dispatch_get_main_queue()) {
                // update some UI
            }
        }
    }
}
