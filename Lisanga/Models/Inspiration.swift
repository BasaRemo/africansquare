//
//  Inspiration.swift
//  RWDevCon
//
//  Created by Mic Pringle on 02/03/2015.
//  Copyright (c) 2015 Ray Wenderlich. All rights reserved.
//

import UIKit
import Parse
import Bond

class Inspiration: PFObject, PFSubclassing {
  
    @NSManaged var name: String?
    @NSManaged var phone_number: String?
    @NSManaged var business_id: String?
    @NSManaged var bio: String?
    @NSManaged var home_country: String?
    @NSManaged var business_name: String?
    @NSManaged var image: PFFile?
    
    var inspirationImage : Observable<UIImage?> = Observable<UIImage?>(nil)
    //MARK: PFSubclassing Protocol
    static func parseClassName() -> String {
        return "Inspiration"
    }
    
    override init () {
        super.init()
        
    }
    
    override class func initialize() {
        var onceToken : dispatch_once_t = 0;
        dispatch_once(&onceToken) {
            // inform Parse about this subclass
            self.registerSubclass()
        }
    }
    
    func downloadImage() {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            // do your task
            self.image?.getDataInBackgroundWithBlock { (data: NSData?, error: NSError?) -> Void in
                if let error = error {
                    print("error downloading image")
                }
                
                if let data = data {
                    let image = UIImage(data: data, scale:1.0)!
                    self.inspirationImage.value = image
                    //                    print("image retrieved")
                }
            }
            dispatch_async(dispatch_get_main_queue()) {
                // update some UI
            }
        }
    }
    
//  class func allInspirations() -> [Inspiration] {
//    var inspirations = [Inspiration]()
//    if let URL = NSBundle.mainBundle().URLForResource("Inspirations", withExtension: "plist") {
//      if let tutorialsFromPlist = NSArray(contentsOfURL: URL) {
//        for dictionary in tutorialsFromPlist {
//          let inspiration = Inspiration(dictionary: dictionary as! NSDictionary)
//          inspirations.append(inspiration)
//        }
//      }
//    }
//    return inspirations
//  }
  
}
