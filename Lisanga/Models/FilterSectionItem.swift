//
//  FilterSectionItem.swift
//  Lisanga
//
//  Created by Professional on 2016-02-15.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit

class FilterSectionItem: NSObject {

    var title: String = ""
    var selected: Bool = false
    var iconPath:String = ""
    
    init(title: String,mIconPath: String) {
        self.title = title
        self.iconPath = mIconPath
    }
    override init() {
    }
}
